## Thesis

This is the code base for the paper “[Color Invariant Agents by Applying Attention in games]()” (will be published
soon.)

## Setup

```cmd
pip install pipenv
pipenv shell
pipenv sync
```

## Examples

In order to test out different settings or explore the possibilities of this repository check out
the [examples](examples)
folder. The following topics are included

- Understanding the Catch environment:
    - [Play the game](examples/human.py)
    - [Explore the options](examples/game.py)


- Testing out attention and augmentations:
    - [Attentions](examples/attention.py)
    - [Augmentations](examples/augmentation.py)


- Playing around with environment colors
    - [Recoloring the environments](examples/recolor.py)
    - [Retrieve the color distributions of gym games](examples/color_distribution.py)



