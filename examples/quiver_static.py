"""
    Example on how to produce Catch visualization quivers
    and visualize them using the Monitor functionality.

    The quiver displays the preferred model action if the `ball` (goal) is
    on that location. An arrow to the left means the left action (1),
    to the right is right (2) and down is Noop (0).

    The arrow is calculated by taking the euclidean distance between the
    difference of (left - right) and (down).The closer that value is to 1
    the more confident the model was in the prediction (the probabilities
    were skewed to one action).

    Notes:
        - If you want to increase the quiver observation,
          make the `screen` size larger while creating the environment.

        - The values in the top and grid will be randomly created (independently)
          when using `RandomAgent`. When using a real model the top values will
          indicate the probability of taking the action (noop, left, right) in
          the current state.

"""
import os

import numpy as np
from gym.wrappers import Monitor

from core.environment import create_env
from core.learn.imitation.agents import ImitationAgent
from core.learn.imitation.models import NeuralNetwork
from core.learn.random.agents import RandomAgent
from core.run import run_imitation
from core.vis.video import VideoReader
from core.wrappers.quiver import QuiverWrapper


def use_video_reader(path, output=None, fps=20, show=True):
    """
        Video renderer to read video files created by the Monitor wrapper.
        It also has the option to change the frame rate, which is required
        to display the short games of `Catch` completely. Otherwise the last
        frames will often be dropped by your viewer.

    :param path: str
        Path to the video (.mp4)
    :param fps: int
        Frame rate with which you read or convert the video if an output is given.
    :param output: str
        Path to the output video (.mp4)
    :param show: bool
        If True will show the video at the given fps if no output is provided.
        Otherwise it will show the resulting output video.
    """

    reader = VideoReader(path=path)

    if output:
        reader.change_fps(output=output, fps=fps)
        reader.path = output

    if show:
        reader.show(fps=fps)


def record_quiver(env, model, nr_games=1, return_image='both', ):
    """
        Record a video using the quiver wrapper.

        :param env: gym.Env
        :param model: core.models
        :param nr_games: int
            Number of games to be recorded.
        :param return_image: str

            Mode that determines which value gets returned by the QuiverWrapper and recorded in the video.
            The valid options are:

                obs: records the observations as called by `env.render('rgb_array')`.
                quiver: records the quiver plots.
                both: records the concatenation of the observations and quiver plots.

        :return: str
            path to video
    """

    env = QuiverWrapper(env, model, return_image=return_image)
    env = Monitor(env, directory='data/recording', force=True, mode='evaluation')

    for episode in range(1, nr_games + 1):
        env.reset()
        done = False
        steps = score = 0
        while not done:
            action = model.compute_action(np.expand_dims(env.render('rgb_obs'), axis=-1))
            obs, reward, done, info = env.step(action)
            score += reward
            steps += 1
            print(f"\r\tRecording episode {episode:2d} / {nr_games}, steps {steps:2d}", end='')
        print(f"\rFinished recording episode {episode:2d}, score {score:2d}")

    env.close()
    return [video for video, json in env.videos]


if __name__ == '__main__':
    # Note the above code, assumes a frame stack 1.
    env, settings = create_env(display='gray', frame_stack=1, training=False)
    example_imitation = False  # If False, use Random model, else imitation model

    if example_imitation:

        model = NeuralNetwork(
                input_shape=env.observation_space.shape,
                output_shape=env.action_space.n,
                learning_rate=5e-4,
                use_cnn=True,
                cnn_number_of_maps=(32, 16),
                cnn_kernel_size=(4, 2),
                cnn_kernel_stride=(2, 1),
                mlp_n_hidden=(),
                mlp_activation_f="tanh",
                mlp_value_n_hidden=(32, 32, 64),
                mlp_value_activation_f="tanh",
                output_activation_f='softmax',
        )

        agent = ImitationAgent(
                env=env,
                model=model,
                nr_games=300,
                minimal_score=1,
                progressbar=True,
                action_func=lambda x: env.optimal_action,
        )

        history, training = run_imitation(
                env=env,
                agent=agent,
                runs=100,
                plot_stats=None,
                plot_period=50,
                fit_kwargs=dict(epochs=20),
        )
    else:
        # This is a random agent.
        agent = RandomAgent(env)

    # Performs a recording of the quiver plot and the observation, side by side.
    paths = record_quiver(env, agent, return_image='both', nr_games=1)

    # Save a slower version of the video, and show it
    for path in paths:
        output = os.path.join(os.path.dirname(path), 'slow', os.path.basename(path))
        use_video_reader(path, output, fps=1, show=True)
