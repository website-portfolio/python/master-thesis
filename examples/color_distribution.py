"""
    Displaying how to found the colors used in `gym` based game.

    The example shows how to get the color distribution for a 2D image
    and display it side by side with the original image.
"""

import gym
import plotly.graph_objs as go
from plotly.subplots import make_subplots

from core.vis.colors_distribution import ColorsDistribution


def plot_dist_image(dist, image):
    trace_dist = dist.figure(return_trace=True)
    trace_image = go.Image(z=image)

    figure = make_subplots(rows=1, cols=2, specs=[[{'type': 'surface'}, {'type': 'image'}]])
    figure.add_trace(trace_dist, row=1, col=1)
    figure.add_trace(trace_image, row=1, col=2)
    figure.show()


def show_image(env, steps=50):
    """
        Displays the color distribution over a number of steps,
        because not all colors are in the image at the start.
    """
    images = []
    image = env.reset()
    for _ in range(steps):
        images.append(image)
        action = env.action_space.sample()
        image, reward, done, info = env.step(action)
        if done: break

    dist = ColorsDistribution(data=images)
    plot_dist_image(dist, image)


if __name__ == '__main__':
    env = gym.make('Pong-v0')

    # Display the first observation and the image side by side.
    show_image(env, steps=1)

    # Display the distribution over a number of steps.
    show_image(env, steps=50)
