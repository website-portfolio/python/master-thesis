"""
    In case you want to try out a Catch environment yourself.
    You can run this code, to play the game.

    It will display the game reward in a separate window.

    Controls:
        - Using the left and right arrow keys
        - Using the `a` and `d` key off the keyboard.

"""
import pprint

import pygame
from gym.utils.play import play, PlayPlot

from core.envs.catch import CatchEnv

KEYS_TO_ACTION = {
    (32,): 0,  # Space (NOOP)

    (pygame.K_LEFT,): 1,  # Left Key
    (ord('a'),): 1,

    (pygame.K_RIGHT,): 2,  # Right Key
    (ord('d'),): 2,

    (pygame.K_UP,): 3,  # Up key
    (ord('w'),): 3,

    (pygame.K_DOWN,): 4,  # Down Key
    (ord('s'),): 4,
}

# Required for callback.
score = 0
episode = 1


def callback(obs_t, obs_tp1, action, reward, done, info):
    global score, episode
    score += reward

    if done:
        print(f"Game {episode:5d} done, score {score}")
        score = 0
        episode += 1

    return [reward, ]


plotter = PlayPlot(callback, 30 * 5, ["reward"])

if __name__ == '__main__':
    env = CatchEnv(display='gray', reset='random')

    # This shows the available display, render and reset modes.
    print("\n", pprint.pformat(env.metadata, indent=1), end='\n\n')

    play(env, fps=30, keys_to_action=KEYS_TO_ACTION, callback=plotter.callback)
