"""

    Examples on data augmentation operations.
    You can continue by pressing any key.
"""

import cv2
import gym

from core import utilities
from core.wrappers.augmentation import Augmentation


def put_text(img, text, loc):
    cv2.putText(img, text, loc, cv2.FONT_HERSHEY_COMPLEX, 1, (255, 255, 0))


def display(env: gym.Env, n_samples=16, size=None, delay=1, window_name='Result'):
    states = [env.reset()]
    for _ in range(n_samples - 1):
        state, reward, done, info = env.step(env.action_space.sample())
        states.append(state.copy())
        if done: env.reset()

    result = utilities.stack_images(images=states, size=size)
    put_text(result, "Press any key", (int(result.shape[0] / 2.5), int(result.shape[1] / 2.5)))

    cv2.imshow(window_name, result)
    cv2.waitKey(delay=delay)
    cv2.destroyWindow(window_name)
    return result


if __name__ == '__main__':
    env = gym.make('CatchRandom-20-20-v2')
    # env = gym.make('Alien-v0')

    for augmentation in [
        'crop',
        'flip',
        'cutout',
        'cutout_color',
        'color_jitter',
        'translate',
        'gray',
        ['gray', 'flip'],
        ['crop', 'flip', 'cutout', 'cutout_color', 'color_jitter', 'translate', 'gray']
    ]:
        venv = Augmentation(
                env=env,
                crop=18,
                gray_p=.3,
                flip_p=.2,
                rotation_p=.3,
                cutout=(2, 5),
                cutout_value=0,
                cutout_color=(2, 5),
                color_jitter=dict(p=.2, hue=(0, 255), saturation=(0, 255), value=(0, 255)),
                translate=dict(p=.2, dx=(-10, 10), dy=(-10, 10)),
                order=[augmentation] if isinstance(augmentation, str) else augmentation,
        )

        display(venv, size=(250, 250), delay=0, window_name=f"Augmentation: {augmentation}")
