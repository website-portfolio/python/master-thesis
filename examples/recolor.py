"""
    Display how the recoloring wrapper can be used on the environments.

    There are two recoloring wrappers, based on:

        - Color: Available for all visual games (gray and color)
        - Position: Only available for custom environments that use Sprite objects.
"""

import time
from collections import namedtuple
from typing import Dict

import gym

from core import CatchEnv
from core.envs.utils.constants import Colors
from core.wrappers.recoloring import RecolorWrapper

rgb = namedtuple('rgb', ('r', 'g', 'b'))


def run_recoloring(env, nr_games=10, sleep=0.1):
    for episode in range(nr_games):
        env.reset()
        done = False
        score = 0
        frames = 0
        while not done:
            obs, reward, done, info = env.step(env.action_space.sample())
            score += reward
            frames += 1
            if sleep is not None: time.sleep(sleep)

            env.render(mode='recolor', size=(400, 400), delay=1)
            print(f"\r\tPlaying game, frame: {frames}, score: {score}", end='')
        print(f"\rFinished game in {frames} frames and a score of {score: 6.2f}")
    env.close()


def example_pong(nr_games=1, new_color=Colors.WHITE):
    # Enemy, player, ball (and end of field stripes)
    pong_colors = rgb(213, 130, 74), rgb(92, 186, 92), rgb(236, 236, 236)

    env = gym.make('Pong-v0')
    env = RecolorWrapper(env, update_colors={old_color: new_color for old_color in pong_colors})
    run_recoloring(env, nr_games)


def example_catch(nr_games=1, update: Dict[str, Colors] = None, new_color=None):
    env = CatchEnv()

    # We can change the specific sprite directly in catch.
    for sprite_name, color in (update or dict()).items():
        env.sprites[sprite_name].color = color

    # We can update colors based on their current value
    env = RecolorWrapper(env, update_colors={env.player.color: new_color or env.player.color})
    run_recoloring(env, nr_games)


if __name__ == '__main__':
    # Pong example of how recoloring looks.
    # The paddles ball and end of field stripes will be colored.
    # in order to find out what other colors there are you can use `color_distribution`.
    example_pong(nr_games=1, new_color=Colors.BLUE)

    # Catch example of how recoloring looks.
    # Catch can generate recoloring based on the 'sprite' (update) and the 'color' (new_color).
    example_catch(nr_games=1, new_color=Colors.BLUE, update=dict(goal=Colors.RED))
