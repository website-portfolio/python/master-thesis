"""
    Example on how to produce Catch Recoloring quivers
    and visualize them using the Monitor functionality.

    Notes:
        - All values are randomly generated, this includes the prediction
          values on the top of the screen and quiver positions. Hence the action
          and quiver are not related.

        - If you want to increase the quiver observation,
          make the `screen` size larger when creating the environment.

        - If you want to ignore a sprite, you can set the step size to 1.
          This will skip the sprite.

"""

from core import CatchEnv
from core.learn.random.models import RandomModel
from core.vis.recoloring import RecoloringQuiverPlot

from core.vis.video import VideoReader

# Color ranges, start, end, number of points
COLOR_RANGES_GRAY = [
    (0, 255, 4),  # Background
    (0, 255, 1),  # Ball (goal)
    (0, 255, 1),  # Paddle (player)
]

COLOR_RANGES_RGB = [
    ((0, 0, 0), (255, 255, 255), (5, 5, 5)),  # Background
    ((0, 0, 0), (255, 255, 255), (5, 5, 5)),  # Ball (goal)
    ((0, 0, 0), (255, 255, 255), (5, 5, 5)),  # Paddle (player)
]

if __name__ == '__main__':
    env: CatchEnv = CatchEnv(display='gray', screen=(800, 800))

    model = RandomModel(env.observation_space.shape, env.action_space.n)
    color_ranges = COLOR_RANGES_GRAY if env.display_mode == 'gray' else COLOR_RANGES_RGB

    sprites = dict(background=env.background, ball=env.goal, paddle=env.player)
    plotter = RecoloringQuiverPlot(env, model, sprites, color_ranges)
    path = plotter.record()

    # read the recorded video, and display it slowly
    video = VideoReader(path=path)
    video.show(fps=1)
