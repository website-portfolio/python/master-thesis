"""
    Validation of models by solving CartPole-v0.
"""

import gym

from core import utilities
from core.learn.qlearning.agents import EpsilonGreedyAgent
from core.learn.qlearning.models import DeepQNetwork, DoubleDQN, DuelingDDQN, DuelingDQN
from core.learn.random.agents import RandomAgent
from core.run import run_experiment
from core.wrappers.epsilon import EpsilonWrapper


def get_network(env, network_type='dqn'):
    """ Retrieve different network types, all networks have been optimized for CartPole.  """

    settings = dict(
            input_shape=env.observation_space.shape,
            output_shape=env.action_space.n,
            learning_rate=0.00025,
            gamma=0.95,
            use_cnn=False,
            mlp_n_hidden=(),
    )

    if network_type.lower() in 'dqn':
        return DeepQNetwork(**settings)

    if network_type.lower() in ['dueling_dqn']:
        return DuelingDQN(**settings)

    if network_type.lower() in ['double_dqn']:
        return DoubleDQN(
                target_network_update_frequency=500,
                **settings
        )
    if network_type.lower() in ['dueling_double_dqn', 'd3qn']:
        return DuelingDDQN(
                target_network_update_frequency=500,
                **settings
        )

    raise ValueError(f"Unknown network type {network_type!r}.")


def run_q_learning(
        env,
        runs=1_000,
        network_type='dqn',
        plot_stats=('score', ('epsilon', 'loss')),
        **kwargs
):
    print(f"\r\n{'*' * 100}\n\n{network_type}\n")

    q_network = get_network(
            env=env,
            network_type=network_type
    )

    agent = EpsilonGreedyAgent(
            env=env,
            model=q_network,
            gamma=0.95,  # discount of future rewards
            batch_size=32,
            replay_capacity=5000,
            training_start=100,  # start training after x number of steps
            training_interval=1,  # train every x steps
            use_epsilon=True
    )

    history = run_experiment(
            env=env,
            agent=agent,
            runs=runs,
            plot_stats=plot_stats,
            plot_period=50,
            desc='\tTraining'.ljust(20),
            **kwargs
    )

    return history


def run_random(env, runs=1_000):
    print(f"\n{'*' * 100}\n\nRandom\n")
    agent = RandomAgent(env)
    history = run_experiment(env, agent, runs=runs, plot_stats=None, plot_period=50)
    return history


if __name__ == '__main__':
    env = gym.make('CartPole-v0')
    env = EpsilonWrapper(
            env,
            epsilon_start=1.0,
            epsilon_end=0.02,
            epsilon_decay=1e-4
    )

    history = run_random(env)
    utilities.describe_history(history, columns=['score'])

    for network_type in ['dqn', 'double_dqn', 'dueling_dqn', 'd3qn']:
        env.reset_epsilon()  # Required to reset the randomness in every stage.
        history = run_q_learning(env, runs=1_000, network_type=network_type, plot_stats=None)
        utilities.describe_history(history, columns=['score'])
