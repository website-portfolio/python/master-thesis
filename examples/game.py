"""
    Displays the original game and the possibility to add extra sprites.
    It is also a small benchmark for the rendering speed of the game.

    Available modes can be found by printing `env.metadata`.
"""

import pprint
import time

import cv2

from core.environment import create_env
from core.envs.utils.constants import Colors
from core.envs.utils.fpstimer import FPSTimer
from core.envs.utils.sprite import Sprite


def show(env, render=True, rgb_array=False, delay=1):
    # Game renderer (will ignore noisy signal)
    if render:
        env.render()

    # Display the rgb array output, this will also show random noise if added.
    if rgb_array:
        cv2.imshow('RGB array', cv2.cvtColor(env.render('rgb_array'), cv2.COLOR_BGR2RGB))
        cv2.waitKey(delay)


def run_example(env, nr_games=50, render=False, render_rgb_array=False, framerate=None):
    """
        Runs the environment for a number of games.
        The game speed depends on the render variables, mode and extra sprites.
        All timing are for `gray` mode, without any additional sprites.
        color mode (without additional sprites) is about ~1.5 times slower.

            +-----------+-----------+---------+
            | render    | rgb array | FPS (~) |
            +===========+===========+=========+
            | True      | True      |     230 |
            +-----------+-----------+---------+
            | False     | True      |     340 |
            +-----------+-----------+---------+
            | True      | False     |   1 600 |
            +-----------+-----------+---------+
            | False     | False     |  30 000 |
            +-----------+-----------+---------+

        :param env: (gym.Env) The environment to be displayed
        :param nr_games: (int) The number of episodes to be played
        :param render: If True, will render the environment using env.render()
        :param render_rgb_array: If True, will render the environment using env.render('rgb_array')
            For CatchEnv(display='noisy'), this will show the added noise.
    """
    start_time = time.perf_counter()
    timer = FPSTimer(1e9)  # More accurate then time.sleep
    steps = 0

    for episode in range(1, nr_games + 1):
        env.reset()
        done = False
        score = 0
        while not done:
            show(env, render=render, rgb_array=render_rgb_array)

            if framerate is not None:
                # Very off for rates higher than 100, better to disable at that point.
                timer.update(framerate)
                timer.sleep()

            action = env.action_space.sample()
            obs, reward, done, info = env.step(action)
            score += reward
            steps += 1

        end_time = time.perf_counter()
        print(f"\rFinished episode: {episode:4d}, score: {score:2d}, fps: {steps / (end_time - start_time):5.0f}")

    env.close()
    cv2.destroyWindow('Output')


def add_sprite(env):
    """
        Example of adding an extra sprite to the game.

        Note:
            The left top of the screen is (0, 0)
    """
    if env.display_mode == 'binary':
        env.add_sprite("Wall", Sprite(x=0, y=1, width=1, height=1, color=1))  # 0 hide,  1 show,

    if env.display_mode == 'gray':
        env.add_sprite("Wall", Sprite(x=0, y=1, width=1, height=1, color=255))  # 0 black, 255 white

    if env.display_mode in ['color', 'noisy']:
        env.add_sprite("Wall", Sprite(x=0, y=1, width=1, height=1, color=Colors.RED))

    return env


if __name__ == '__main__':
    env, settings = create_env(display='gray', gaussian=(0, 5))

    # This shows the available display, render and reset modes.
    print("\n", pprint.pformat(env.metadata, indent=1), end='\n\n')

    # uncomment to add extra sprites
    # env = add_sprite(env)

    # None would be as fast as possible, looses accuracy around >150 fps.
    env.set_fps(10)

    run_example(env, nr_games=50, render=True, render_rgb_array=True)
