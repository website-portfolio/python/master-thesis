import gym

from core import utilities
from core.learn.imitation.agents import ImitationAgent
from core.learn.imitation.models import NeuralNetwork
from core.run import run_imitation

if __name__ == '__main__':
    env: gym.Env = gym.make('CartPole-v0')

    model = NeuralNetwork(
            input_shape=env.observation_space.shape,
            output_shape=env.action_space.n,
            learning_rate=0.00025,
            use_cnn=False,
            cnn_number_of_maps=(32, 16),
            cnn_kernel_size=(4, 2),
            cnn_kernel_stride=(2, 1),
            mlp_n_hidden=(),
            mlp_activation_f="tanh",
            mlp_value_n_hidden=(32, 32, 64),
            mlp_value_activation_f="tanh",
            output_activation_f='softmax',
    )

    agent = ImitationAgent(
            env=env,
            model=model,
            nr_games=50,
            minimal_score=100,
            progressbar=True,
            action_func=None,
    )

    history, training = run_imitation(
            env=env,
            agent=agent,
            runs=100,
            plot_stats=None,
            plot_period=50,
            fit_kwargs=dict(epochs=20),
    )

    utilities.describe_history(history, columns=['score'])
    utilities.plot_history(training.history, columns=['loss'], block=True)
