"""
    Displaying how attention wrappers can be applied to increase agent attention
    at certain regions. The focussing of attention can be done using either
    Color or Positions.

    Note:
         The position wrapper is currently only available for custom environments.
         This is because the position is directly extracted from the environment sprites.

"""

import time
from collections import namedtuple
from typing import Union

import gym

from core import CatchEnv
from core.wrappers.attention import AttentionColorWrapper, AttentionPositionWrapper, DoubleAttentionWrapper

rgb = namedtuple('rgb', ('r', 'g', 'b'))


def run_attention(env, nr_games=10, sleep: Union[None, float, int] = 0.1):
    for episode in range(nr_games):
        env.reset()
        done = False
        score = 0
        frames = 0
        while not done:
            obs, reward, done, info = env.step(env.action_space.sample())
            score += reward
            frames += 1
            if sleep is not None: time.sleep(sleep)

            env.render(mode='attention', delay=1)
            print(f"\r\tPlaying game, frame: {frames}, score: {score}", end='')
        print(f"\rFinished game in {frames} frames with a score of {score: 6.2f}")
    env.close()


def example_pong(nr_games=1, sleep: Union[None, float, int] = 0.1):
    # Enemy, player, ball (and end of field stripes)
    pong_colors = rgb(213, 130, 74), rgb(92, 186, 92), rgb(236, 236, 236)

    env = gym.make('Pong-v0')

    # Pong requires the AttentionColor wrapper,
    # because there is no specific player location information at the start.
    env = AttentionColorWrapper(env, fwhm=15, colors=pong_colors, cluster=True)

    run_attention(env, nr_games, sleep=sleep)


def example_catch(env=CatchEnv(), nr_games=1, sleep=0.1, attention='position', fwhm=3):
    # We can take the specific player locations and color from the environment.
    # Therefore Catch can use AttentionColor and AttentionPosition wrapper.
    if attention == 'color':
        env = AttentionColorWrapper(env, colors=[env.goal.color, env.player.color], fwhm=fwhm, cluster=False)
    elif attention == 'position':
        env = AttentionPositionWrapper(env, sprites=[env.goal, env.player], fwhm=fwhm, )
    elif attention == 'double':
        env = DoubleAttentionWrapper(env=env, colors=[env.goal.color], sprites=[env.player], fwhm=fwhm, cluster=False)
    else:
        raise ValueError(f"Unknown attention type {attention!r}")

    run_attention(env, nr_games, sleep=sleep)


if __name__ == '__main__':
    """
        When running you will see three images:
            - On the left the new image, with the attention filter applied
            - In the center the original image
            - On the right the applied attention centers.
    """

    # Pong example of how visual attention looks.
    example_pong(nr_games=1, sleep=None)

    # Catch example of how visual attention looks like.
    # Catch can generate attention based on the 'position', 'color' and combination.
    env = CatchEnv(display='gray')

    example_catch(env=env, nr_games=5, attention='color', sleep=0.1)
    example_catch(env=env, nr_games=5, attention='position', sleep=0.1)

    """
            When running you will see 6 images:
            
            On the top
                - left the new image, with the attention filter applied
                - center the original image
                - right the combined attention distribution max(color, position)
                
            On the bottom
                - left the color attention distribution
                - center the positional attention distribution
                - right empty fill image
    """
    example_catch(env=env, nr_games=5, attention='double', sleep=0.1)
