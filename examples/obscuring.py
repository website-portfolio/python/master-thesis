"""
    Displays how to obscure an environment with some prepared patterns.

"""

from core.envs.utils.constants import Colors
from core.envs.utils.obscure import *
from examples.game import run_example


def get_color(env):
    """
        Example of adding an extra sprite to the game.

        Note:
            The left top of the screen is (0, 0)
    """
    if env.display_mode == 'binary':
        return 1  # 0 hide,  1 show,
    if env.display_mode == 'gray':
        return 255  # 0 black, 255 white
    if env.display_mode in ['color', 'noisy']:
        return Colors.WHITE
    raise ValueError(f'Unkown display mode {env.display_mode!r}')


if __name__ == '__main__':
    _env = CatchEnv(display='color', grid=(21, 21))

    settings = {
        'nr_games': 1,
        'render': True,
        'render_rgb_array': True,
        'framerate': 10,
    }

    env = obscure_raster(_env, padding=2, color=get_color(_env))
    run_example(env, **settings)

    env = obscure_horizontal_lines(_env, padding=2, color=get_color(_env))
    run_example(env, **settings)

    env = obscure_vertical_lines(_env, padding=2, color=get_color(_env))
    run_example(env, **settings)

    # Optional patterns `raster`, `vertical`, `horizontal`.
    # env = obscure(_env, pattern=None, color=get_color(_env))
    # run_example(env, **settings)
