import abc


class Agent:
    """ Interface for the agent class """
    stats = {}

    @abc.abstractmethod
    def get_action_probabilities(self, state):
        """ Return the action probabilities for a specific state.  """

    @abc.abstractmethod
    def compute_action(self, state):
        """ Return the action for the environment.  """

    @abc.abstractmethod
    def update(self, *args, **kwargs):
        """ Gets called after every step.  """

    @abc.abstractmethod
    def finalize_episode(self, *args, **kwargs) -> dict:
        """ Return the statistics of the agent.  """


class Models:
    """ Interface for the Models class """

    @abc.abstractmethod
    def get_action_probabilities(self, state):
        """ Return the action predictions for a state.  """

    @abc.abstractmethod
    def get_best_action(self, state):
        """ return the best action for the provided state.  """

    @abc.abstractmethod
    def update(self, *args, **kwargs):
        """ Gets called after every step.  """

    @abc.abstractmethod
    def save(self, save_folder):
        """ Perform the saving of a model based on a provided folder.  """

    @abc.abstractmethod
    def load(self, load_folder):
        """ Perform the loading of a model based on a provided folder.  """
