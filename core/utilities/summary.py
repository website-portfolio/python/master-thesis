from typing import Dict

import matplotlib.pyplot as plt
import plotly.graph_objs as go

from core.wrappers.attention import *


def plot_summary(result, sprites: Dict[str, Sprite], metric='mean', show=True, fig=None, axs=None, min_score=None):
    """ Shows subplots of all the sprites and the score per color.  """
    summary = result.copy() if min_score is None else result[result.score >= min_score].copy()

    if axs is not None:
        return _plot_summary_gray(summary, metric, sprites, show, fig=fig, axs=axs)
    if all(color in result.columns for color in 'rgb'):
        return _plot_summary_color(summary, metric, sprites, show)
    raise ValueError(f"Unknown color type")


def _plot_summary_gray(result, metric, sprites, show, fig, axs, ):
    means = result.filter(regex=metric, axis=0)

    if axs is None:
        fig, axs = plt.subplots(nrows=len(sprites), figsize=(10, 5 * len(sprites)))
        axs = [axs] if len(sprites) == 1 else axs

    for sprite, ax in zip(sprites, axs):
        result[['r', 'g', 'b']] = result[sprite]
        result['color'] = result.r * 0.2989 + result.g * 0.5870 + 0.1140 * result.b  # Convert colors to grayscale.

        values = means.loc[means['sprite'] == sprite]
        values.plot.line(x='color', y='score', style='-o', ax=ax)

        ax.set_xlabel('Color')
        ax.set_ylabel('Reward')
        ax.set_title(f"{sprite}")
        ax.set_xlim(0, 256)
        ax.set_ylim(0, 1)

    plt.subplots_adjust(hspace=0.5)
    if show: plt.show()
    return fig


def _plot_summary_color(results, metric, sprites, show):
    data = results.filter(regex=metric, axis=0).copy()
    data['rgb'] = data.apply(lambda row: f"rgb({row.r},{row.g},{row.b})", axis=1, result_type='expand')

    # Generate colormap
    cmap = utilities.truncate_colormap(cmap=plt.get_cmap('jet'), min_val=0, max_val=0.5)
    colorscale = [[i / 256, f"rgb{tuple(map(lambda x: int(x * 256), cmap(i)[:3]))}"] for i in range(256 + 1)]

    figures = []
    for sprite in sprites:
        result = data[data['sprite'] == sprite]

        trace = go.Scatter3d(
                x=result.r, y=result.g, z=result.b,
                hovertemplate="Color: %{customdata[0]}<br>"
                              "Score: %{customdata[1]}<extra></extra>",
                customdata=result[['rgb', 'score']],
                hoverlabel=dict(bgcolor=result.rgb),
                marker=dict(
                        color=result.score, opacity=0.6, colorscale=colorscale,
                        size=result.score * 15,
                        colorbar=dict(tick0=0, dtick=0.2, x=0.8),
                        autocolorscale=False, cmin=0, cmax=1),
                mode='markers',

        )

        fig = go.Figure(data=trace, layout=dict(showlegend=False, title=sprite))
        fig.update_layout(scene=dict(
                aspectratio=dict(x=1, y=1, z=1),
                **{f"{ax}axis": dict(title=label, range=(0, 260), dtick=50, autorange=False)
                   for ax, label in zip('xyz', 'rgb')}, ),
        )

        figures.append(fig)
        if show: fig.show()

        fig = Gaussian.plot1d(result.score, vmin=1e-6, vmax=1, cmap=cmap, show=False)
        fig.gca().set_xlabel("color idx")
        fig.gca().set_ylabel(f"{metric} score")
        figures.append(fig)
        if show: plt.show()

    return figures
