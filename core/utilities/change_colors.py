import itertools
from typing import Dict, List, Union

import numpy as np

from core import CatchEnv
from core.envs.utils.constants import Colors
from core.envs.utils.sprite import Sprite


def set_sprite_colors(env: CatchEnv, sprites: Dict[str, Sprite], color_ranges: List[Union[int, List[int]]]):
    """
         Goes through a combination of colors_settings for game sprites.

         This generator is exhausted when all possible color range combinations have been applied
         on all the sprites separately. After exhaustion of the generator the environment will
         be returned to the original states (all sprites color restored to entry point).

         :param env: CatchEnv
             The environment on which the sprites will be changing.

         :param sprites: Dict[str, Sprite]
             The sprites for which the colors_settings have to be altered.

         :param color_ranges: List[Union[int, List[int]]]
             The new color ranges that have to be applied to the sprites.
             The order of the color rotations is the order that will be applied to the sprites.
             Note that the color ranges are applied sequential, so first one wheel is exhausted
             before the next sprite color is altered.

             The color_rotation has to contain the `start`, `end` and `step size` for every value.
             If this is a grayscale value, all of them have to be integers, while for RGB and RGBA
             they each need to  have the same length of 3 for RGB values and 4 for RGBA values.
    """
    old_colors = [sprite.color for sprite in sprites.values()]

    if env.display_mode == 'gray':
        colors = [np.linspace(*settings, dtype=np.uint8) for settings in color_ranges]
    else:
        colors = [get_multi_colors(*settings) for settings in color_ranges]

    for sprite, combinations, old_color in zip(sprites.values(), colors, old_colors):
        for color in combinations:
            sprite.color = color
            yield sprites
            sprite.color = old_color


def set_multi_sprite_colors(env: CatchEnv, sprites: Dict[str, Sprite], color_ranges: List[Union[int, List[int]]]):
    """
         Goes through a combination of colors_settings for game sprites.

         This generator is exhausted when all possible color range combinations have been applied
         on all sprites. After exhaustion of the generator the environment will
         be returned to the original states (all sprites color restored to entry point).

         :param env: CatchEnv
             The environment on which the sprites will be changing.

         :param sprites: Dict[str, Sprite]
             The sprites for which the colors_settings have to be altered.

         :param color_ranges: List[Union[int, List[int]]]
             The new color ranges that have to be applied to the sprites.
             The order of the color rotations is the order that will be applied to the sprites.
             Note that the color ranges are applied sequential, so first one wheel is exhausted
             before the next sprite color is altered.

             The color_rotation has to contain the `start`, `end` and `step size` for every value.
             If this is a grayscale value, all of them have to be integers, while for RGB and RGBA
             they each need to have the same length of 3 for RGB values and 4 for RGBA values.
    """
    old_colors = [sprite.color for sprite in sprites.values()]

    if env.display_mode == 'gray':
        colors = itertools.product(*[np.linspace(*settings, dtype=np.uint8) for settings in color_ranges])
    else:
        colors = itertools.product(*[get_multi_colors(*settings) for settings in color_ranges])

    for combination in colors:
        for sprite, color in zip(sprites.values(), combination):
            sprite.color = color
        yield sprites

    for sprite, color in zip(sprites.values(), old_colors):
        sprite.color = color


def get_multi_colors(starts=(0, 0, 0), ends=(256, 256, 256), nums=(2, 2, 2)):
    """ Generate all combinations of the input ranges.  """
    assert len(starts) == len(ends) == len(nums), f"{len(starts)}, {len(ends)}, {len(nums)}"

    channels = [np.linspace(*settings, dtype=np.uint8) for settings in list(zip(starts, ends, nums))]
    for color in itertools.product(*channels):
        yield Colors(*color)
