import base64
import io
from typing import Callable, List

import cv2
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import tqdm
import tqdm.notebook
from PIL import Image
from matplotlib.colors import LinearSegmentedColormap
from plotly.graph_objs import Figure

from core.vis.stats_plotter import StatsPlotter


def truncate_colormap(cmap: plt.cm, min_val=0., max_val=1., n=None) -> Callable:
    """ Truncate a known colormap to a smaller range.  """
    n = n if n is not None else cmap.N
    name = f"trunc({cmap.name}, {min_val:.2f}, {max_val:.2f})"
    cmap_colors = cmap(np.linspace(min_val, max_val, n))
    my_cmap = LinearSegmentedColormap.from_list(name=name, colors=cmap_colors)
    return my_cmap


def fig_to_numpy(fig, dpi=180):
    """
    Converts an input Figure, to a numpy array.
    If used by Matplotlib, this will close the figure.

    :param fig: plt.figure
        The input figure, with all items drawn onto it.
    :param dpi: int
        The resolution of the output image, keep in mind that larger
        takes longer.
    :return: np.ndarray
        Return a numpy array containing the figure images.
    """
    if isinstance(fig, Figure):
        fig_bytes = fig.to_image(format="png", engine='kaleido')
        buf = io.BytesIO(fig_bytes)
    else:
        buf = io.BytesIO()
        fig.tight_layout()
        fig.savefig(buf, format="png", dpi=dpi)
        plt.close(fig=fig)

    buf.seek(0)
    img_arr = np.frombuffer(buf.getvalue(), dtype=np.uint8)
    buf.close()
    img = cv2.imdecode(img_arr, 1)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    return img


def arr_to_b64(array, ext='jpeg'):
    """ Convert an array to an image source that can be displayed."""
    image = Image.fromarray(array)
    if ext == 'jpg': ext = 'jpeg'
    if ext == 'jpeg' and image.mode in ('RGBA', 'A'):
        background = Image.new(image.mode[:-1], image.size, (255, 255, 255))
        background.paste(image, image.split()[-1])
        image = background

    buffer = io.BytesIO()
    image.save(buffer, format=ext)
    encoded = base64.b64encode(buffer.getvalue()).decode('utf-8')

    return f'data:image/{ext};base64, ' + encoded


def stack_images(images, size=None, rows=None, columns=None):
    """
    Stack paths to be displayed as one output, can handle BGR and gray paths.

    :param images: List[np.ndarray]
        List of paths we are going to stack together.

    :param size: (int, int)
        Output dimensions of every image (width, height), if None,
        the size of the first image will be taken.

    :param rows:  int
        Number of paths per row, if None it will be automatically calculated
        if columns is given. Otherwise it creates the most squared shape that it can get.

    :param cols: int
        Number of paths per column, if None it will be automatically calculated
        if rows is given. Otherwise it creates the most squared shape that it can get.

    :return: np.ndarray
        A single image containing a stack of all the other images.
    """

    # Calculate rows and columns based on input data.
    if rows is None and columns is None:
        rows = columns = int(np.ceil(np.sqrt(len(images))))
    if columns is None:
        columns = int(np.ceil(len(images) / rows))
    if rows is None or len(images) <= (rows - 1) * columns:  # Remove unnecessary rows
        rows = int(np.ceil(len(images) / columns))

    # retrieve width and height for every image if not provided
    height, width = images[0].shape[:2] if size is None else reversed(size)

    # Add extra paths if not enough to create the rows * columns shape
    [images.append(np.zeros((height, width, 3), dtype=np.uint8)) for _ in range(rows * columns - len(images))]

    # Create the combined image shape
    for idx in range(len(images)):
        if images[idx].shape[:2] != (height, width):
            images[idx] = cv2.resize(images[idx], (width, height), interpolation=cv2.INTER_NEAREST_EXACT)

        if len(images[idx].shape) != 3:
            images[idx] = cv2.cvtColor(images[idx], cv2.COLOR_GRAY2BGR)

        if images[idx].shape[-1] == 4:
            images[idx] = cv2.cvtColor(images[idx], cv2.COLOR_BGRA2BGR)

    merge_columns = [np.hstack(images[i:i + columns]) for i in range(0, len(images), columns)]
    merged = np.vstack(merge_columns) if len(merge_columns) > 0 else merge_columns.pop()
    return merged


def normalize(state, normalize=True):
    state = np.array(state)
    if normalize:
        if state.dtype == np.bool:
            return state
        if state.dtype == np.uint8:
            return state.astype("float32") / 255.0
    return state


def get_progressbar(runs, plot_stats, ipython=False, desc='', leave=False, position=0, **kwargs):
    if plot_stats is not None: return range(runs)
    if ipython: return tqdm.notebook.trange(runs, position=position, desc=desc, leave=leave, **kwargs)
    return tqdm.trange(runs, position=position, desc=desc, leave=leave, **kwargs)


def describe_history(history, columns: List[str] = None, max_columns=None, max_width=320, show=True, **kwargs):
    """
        Describe a history object.

    :param history: (dict) The history stats.
    :param columns: (List[str]) Columns that have to be described.
    :param max_columns: (int) The maximum number of columns per row.
    :param max_width: (int) The maximum characters per line.
    :return: (pd.DataFrame)
        Dataframe of the described history.
    """
    df = pd.DataFrame(history)
    pd.set_option('display.max_columns', max_columns)
    pd.set_option('display.width', max_width)

    summary = df[columns or df.columns].describe(**kwargs)
    if show: print(f"\n{summary.T}", end='\n\n')
    return summary


def plot_history(history, x_plot=None, columns: List[str] = None, ipython=False, block=False):
    plotter = StatsPlotter(x_plot=x_plot, plot_stats=columns or history.keys(), history=history)
    plotter.show(ipython=ipython, block=block)
