import json
import os
import time
from typing import Union

import gym
import numpy as np

from core import utilities, CatchEnv
from core.interface import Agent
from core.learn.imitation.agents import ImitationAgent
from core.learn.qlearning.agents import EpsilonGreedyAgent
from core.learn.qlearning.models import DeepQNetwork, DuelingDQN, DoubleDQN, DuelingDDQN
from core.vis.stats_plotter import StatsPlotter


def run_episode(
        env: Union[gym.Env, CatchEnv],
        agent: Agent,
        train=True,
        step_limit=100_000,
        render=False,
        sleep=0.1,
        mode='human'
):
    done = False
    score = 0
    steps = 0
    timer = time.time()
    state = env.reset()

    while not done and steps < step_limit:
        action = agent.compute_action(state)
        next_state, reward, done, info = env.step(action)
        score += reward

        # Clip the agent's reward - improves stability
        reward = np.clip(reward, a_min=-1, a_max=1, dtype=np.float32)

        if train:
            agent.update(state, action, reward, next_state, done)

        if render:
            env.render(mode=mode)
            time.sleep(sleep)

        state = next_state
        steps += 1

    stats = agent.finalize_episode()
    env.close()

    return {
        "score": score,
        "steps_per_game": steps,
        "framerate": steps / (time.time() - timer + 1e-8),
        **stats
    }


def run_experiment(
        env: Union[gym.Env, CatchEnv],
        agent: Agent,
        runs=100,
        x_plot=None,
        plot_stats=(),
        plot_period=1,
        ipython=False,
        history=None,
        desc='',
        **kwargs,
):
    """
       :param env: (gym.Environment)
       :param agent: (object) an object that implements compute_action() and update() methods
       :param runs: (int) number of episodes
       :param plot_stats: (array_like) a list of elements to plot from a history
       :param ipython: (bool) if True will render using the IPython display, otherwise matplotlib
       :param history:  (dict) history object to be able to call this function
        multiple times without loosing the data about the training process
       :return: (dict) history
    """
    plotter = StatsPlotter(x_plot, plot_stats, plot_period, history)
    progressbar = utilities.get_progressbar(runs, plot_stats=plot_stats, ipython=ipython, desc=desc)

    for episode in progressbar:
        stats = run_episode(env, agent, **kwargs)
        plotter.update_stats(stats)
        plotter.show(episode=episode, ipython=ipython)

    plotter.show(episode=runs, ipython=ipython)
    plotter.close()
    return plotter.history


def run_imitation(
        env: Union[gym.Env, CatchEnv],
        agent: ImitationAgent,
        runs=100,
        x_plot=None,
        plot_stats=(),
        plot_period=1,
        ipython=False,
        history=None,
        fit_kwargs=None,
        desc='',
        **kwargs,
):
    """
       :param env: (gym.Environment)
       :param agent: (object) an object that implements compute_action() and update() methods
       :param runs: (int) number of episodes
       :param plot_stats: (array_like) a list of elements to plot from a history
       :param ipython: (bool) if True will render using the IPython display, otherwise matplotlib
       :param history:  (dict) history object to be able to call this function
        multiple times without loosing the data about the training process
       :return: (dict) history
    """

    # Perform training
    training_history = {}
    if not agent.trained:
        while not agent.done_collecting:
            run_episode(env, agent, train=True)
        training_history = agent.fit(**(fit_kwargs or dict()))

    # Perform evaluation
    history = run_experiment(env, agent, runs, x_plot, plot_stats, plot_period, ipython, history, desc, **kwargs)
    return history, training_history


def run_q_learning(
        env: Union[gym.Env, CatchEnv],
        runs=1_000,
        network='dqn',
        plot_stats=None,
        plot_period=50,
        use_epsilon=False,
        ipython=False,
        **kwargs
):
    settings = {k: v for k, v in locals().items() if k not in ['env', 'kwargs']}
    settings.update(**kwargs)

    q_network = get_network(env, network=network)

    agent = EpsilonGreedyAgent(
            env=env,
            model=q_network,
            gamma=0.95,  # discount of future rewards
            batch_size=32,
            replay_capacity=5_000,
            training_start=100,  # start training after x number of steps
            training_interval=1,  # train every x steps
            use_epsilon=use_epsilon,
    )

    history = run_experiment(
            env=env,
            agent=agent,
            runs=runs,
            plot_stats=plot_stats,
            plot_period=plot_period,
            ipython=ipython,
            **kwargs
    )

    return agent, history, settings


def get_network(env: Union[gym.Env, CatchEnv], network: str):
    """ Retrieve different network types, all networks have been optimized for CatchEnv.  """
    network = network.lower()
    settings = dict(
            input_shape=env.observation_space.shape,
            output_shape=env.action_space.n,
            learning_rate=1e-4,
            gamma=0.95,
            use_cnn=True,
            cnn_number_of_maps=(32, 16),
            cnn_kernel_size=(4, 2),
            cnn_kernel_stride=(2, 1),
            mlp_n_hidden=(32, 64, 64),
            mlp_activation_f="tanh",
            mlp_value_n_hidden=(32, 32, 64),
            mlp_value_activation_f="relu",
    )

    if network in 'dqn':
        return DeepQNetwork(**settings)

    if network in ['dueling_dqn']:
        return DuelingDQN(**settings)

    if network in ['double_dqn']:
        return DoubleDQN(target_network_update_frequency=500, **settings)

    if network in ['dueling_double_dqn', 'd3qn']:
        return DuelingDDQN(target_network_update_frequency=500, **settings)

    if network in ['d2qn', '2dqn', 'ddqn']:
        raise ValueError(f"Ambiguous naming {network!r}, valid ['dueling_dqn', 'double_dqn']")

    raise ValueError(f"Unknown network type {network!r}.")


def load_history(path: str):
    with open(path, 'r') as file:
        history = json.load(file)
    return history


def save_history(history: list, path: str):
    if not os.path.exists(os.path.dirname(path)):
        os.makedirs(os.path.dirname(path))

    with open(path, 'w') as file:
        json.dump(history, file)
