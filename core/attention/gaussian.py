import matplotlib.cm as mpl_cm
import matplotlib.colors as mpl_colors
import matplotlib.pyplot as plt
import numpy as np
from scipy.spatial.distance import cdist


class Gaussian:
    def __init__(self, size):
        self.size = size
        self.center = np.array(self.size) / 2
        self.axis = self._calculate_axis()

    def _calculate_axis(self):
        """
            Generate a list of rows, columns over multiple axis.

            Example:
                Input: size=(5, 3)
                Output: [array([0, 1, 2, 3, 4]), array([[0], [1], [2]])]
        """
        axis = [np.arange(size).reshape(-1, *np.ones(idx, dtype=np.uint8))
                for idx, size in enumerate(self.size)]
        return axis

    def update_size(self, size):
        """ Update the size and calculate new centers and axis.  """
        self.size = size
        self.center = np.array(self.size) / 2
        self.axis = self._calculate_axis()

    def create(self, dim=1, fwhm=3, center=None):
        """ Generate a gaussian distribution on the center of a certain width.  """
        center = center if center is not None else self.center[:dim]
        distance = sum((ax - ax_center) ** 2 for ax_center, ax in zip(center, self.axis))
        distribution = np.exp(-4 * np.log(2) * distance / fwhm ** 2)
        return distribution

    def creates(self, dim=2, fwhm=3, centers: np.ndarray = (None,)):
        """ Combines multiple gaussian distributions based on multiple centers.  """
        centers = np.array(centers)
        indices = np.indices(self.size).reshape(dim, -1).T

        distance = np.min(cdist(indices, centers, metric='euclidean'), axis=1)
        distance = np.power(distance.reshape(self.size), 2)

        distribution = np.exp(-4 * np.log(2) * distance / fwhm ** 2)
        return distribution

    @staticmethod
    def plot(distribution, show=True):
        """ Plotter, in case you do not know the dimensions of your distribution, or want the same interface.  """
        if len(distribution.shape) == 1:
            return Gaussian.plot1d(distribution, show)
        if len(distribution.shape) == 2:
            return Gaussian.plot2d(distribution, show)
        if len(distribution.shape) == 3:
            return Gaussian.plot3d(distribution, show)
        raise ValueError(f"Trying to plot {len(distribution.shape)}-dimensional data, "
                         f"Only 1D, 2D, and 3D distributions are valid.")

    @staticmethod
    def plot1d(distribution, show=True, vmin=None, vmax=None, cmap=None):
        norm = mpl_colors.Normalize(
                vmin=vmin if vmin is not None else distribution.min(),
                vmax=vmax if vmin is not None else distribution.max()
        )
        cmap = mpl_cm.ScalarMappable(norm=norm, cmap=cmap or mpl_cm.get_cmap('jet'))
        cmap.set_array(distribution)
        c = [cmap.to_rgba(value) for value in distribution]  # defines the color

        fig, ax = plt.subplots()
        ax.scatter(np.arange(len(distribution)), distribution, c=c)
        fig.colorbar(cmap)
        if show: plt.show()
        return fig

    @staticmethod
    def plot2d(distribution, show=True):
        fig, ax = plt.subplots()
        img = ax.imshow(distribution, cmap='jet')
        fig.colorbar(img)
        if show: plt.show()
        return fig

    @staticmethod
    def plot3d(distribution, show=True):
        m, n, c = distribution.shape
        x, y, z = np.mgrid[:m, :n, :c]
        out = np.column_stack((x.ravel(), y.ravel(), z.ravel(), distribution.ravel()))
        x, y, z, values = np.array(list(zip(*out)))

        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')

        # Standalone colorbar, directly creating colorbar on fig results in strange artifacts.
        img = ax.scatter([0, 0], [0, 0], [0, 0], c=[0, 1], cmap=mpl_cm.get_cmap('jet'))
        img.set_visible = False
        fig.colorbar(img)

        ax.scatter(x, y, z, c=values, cmap=mpl_cm.get_cmap('jet'))
        if show: plt.show()
        return fig
