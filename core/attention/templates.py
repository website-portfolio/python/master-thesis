import os
import glob
import logging
import cv2
import seaborn
import numpy as np

import matplotlib.patches as mpatch
import matplotlib.pyplot as plt

from pathlib import Path


class Templates:
    """ Template class

    It loads all templates in a specific folder and pattern.

    After which it can match them to an image.

    :param folder: str
        Main folder of the templates
    :param regex: str
        Matching pattern used to match templates relative from folder position
    """

    def __init__(self, folder: str = 'templates', regex: str = '*'):
        self.folder = folder
        self.regex = regex
        self.templates, self.templates_colors = self.load_templates(folder=folder, regex=regex)

    @staticmethod
    def match_all(image, template, threshold=0.8, debug=False, color=(0, 0, 255)):
        """ Match all template occurrences which have a higher likelihood than the threshold """
        width, height = template.shape[:2]
        match_probability = cv2.matchTemplate(image, template, cv2.TM_CCOEFF_NORMED)
        match_locations = np.where(match_probability >= threshold)

        # Add the match rectangle to the screen
        locations = []
        for x, y in zip(*match_locations[::-1]):
            locations.append(((x, x + width), (y, y + height)))

            if debug:
                cv2.rectangle(image, (x, y), (x + width, y + height), color, 1)
        return locations

    @staticmethod
    def load_templates(folder: str, regex='*'):
        """ Load all templates and add assign a unique color for it.  """
        templates = dict()
        for file in glob.glob(os.path.join(folder, regex)):
            templates[Path(file).stem] = cv2.cvtColor(cv2.imread(file, 0), cv2.COLOR_BGRA2RGB)
        colors_rgb_norm = seaborn.color_palette('bright', n_colors=len(templates))
        colors_rgb = tuple(map(lambda rgb_norm: tuple(map(lambda rgb: int(rgb * 255), rgb_norm)), colors_rgb_norm))
        return templates, colors_rgb

    def locate_templates(self, screenshot):
        # Retrieve all template information
        locations, debug = dict(), logging.root.level <= logging.DEBUG
        for (name, template), color in zip(self.templates.items(), self.templates_colors):
            locations[name] = self.match_all(image=screenshot, template=template, debug=debug, color=color)
        return screenshot, locations

    def show_templates(self, show=False):
        """ PLot all templates with color and name in a figure.  """

        width = 3  # Define number of columns in the grid.
        templates = {Path(file).stem: plt.imread(file) for file in glob.glob(f'{self.folder}/*')}
        fig = plt.figure(figsize=[width, len(templates) + 1])
        ax = fig.add_axes([0, 0, 1, 1])

        # Fix border and center lines
        for num in range(width + 1):
            ax.axvline(num, color='k', linewidth=3 + width * (num == 0 or num == width))

        for num in range(len(templates) + 2):
            ax.axhline(num, color='k', linewidth=3 + width * (num == 0 or num == len(templates) + 1))

        # Add basic color and template preview to
        for idx, ((name, image), color) in enumerate(zip(self.templates.items(), self.templates_colors)):
            patch_color = mpatch.Rectangle((0, idx), 1, 1, color=np.array(color) / 255)
            patch_image = mpatch.Rectangle((1, idx), 1, 1, transform=ax.transData)

            img = ax.imshow(image, extent=[1, 2, idx, idx + 1])
            img.set_clip_path(patch_image)
            ax.add_patch(patch_color)
            ax.text(2.2, idx + 0.5, f"{name}", va='center', fontsize=10, wrap=True)

        ax.text(.5, len(templates) + .5, 'Colour', ha='center', va='center')
        ax.text(1.5, len(templates) + .5, 'Image', ha='center', va='center')
        ax.text(2.5, len(templates) + .5, 'File name', ha='center', va='center')

        ax.set_xlim(0, width)
        ax.set_ylim(0, len(templates) + 1)

        ax.axis('off')
        if show: plt.show()
        return fig

    def update(self):
        self.templates, self.templates_colors = self.load_templates(folder=self.folder, regex=self.regex)
