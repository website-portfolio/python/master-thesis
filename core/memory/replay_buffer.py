import random
from collections import deque

import numpy as np


class ReplayBuffer:
    def __init__(self, replay_capacity=50_000, include_last_samples=3):
        """
        Simple replay buffer based on the collections.deque
        :param replay_capacity: the size of the buffer, i.e. the number of last transitions to save
        :param include_last_samples: include a number of most recent observations into a sample batch
        """
        self.replay_capacity = replay_capacity
        self.include_last = include_last_samples
        self._buffer = deque(maxlen=self.replay_capacity)

    def __len__(self):
        return len(self._buffer)

    @property
    def filled(self) -> float:
        """ return percentage filled.  """
        return "{:6.2f}".format(len(self) / self.replay_capacity * 100)

    def store(self, transition):
        """
        store the transition in a replay buffer
        :param transition:
        :return: None
        """
        self._buffer.appendleft(transition)

    def sample_batch(self, batch_size):
        """
        Sample a batch of transitions from replay buffer
        :param batch_size: size of the sampled batch
        :return: tuple of ndarrays with batch_size as first dimension
        """
        batch = random.sample(self._buffer, batch_size)
        for index in range(self.include_last):
            batch[index] = self._buffer[index]
        return self._batch_to_arrays(batch)

    def _batch_to_arrays(self, batch):
        """
        Transforms list of transition tuples to a tuple of ndarrays
        :param batch: list of tuples of every element in a batch
        :return: tuple of ndarrays
        """
        return [np.stack(data, axis=0) for data in zip(*batch)]


if __name__ == '__main__':
    buffer = ReplayBuffer(replay_capacity=10_000)
    print(buffer.filled, len(buffer))

    for idx in range(10_000):
        buffer.store((idx,))
        print(buffer.filled)

    print(buffer.filled, len(buffer))
    print(buffer.sample_batch(batch_size=32))
