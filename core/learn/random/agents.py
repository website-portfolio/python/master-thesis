import numpy as np

from core.interface import Agent


class RandomAgent(Agent):
    """ Agent taking random actions.  """

    def __init__(self, env):
        self.env = env
        self.input_shape = self.env.observation_space.shape
        self.output_shape = self.env.action_space.n

    def get_action_probabilities(self, state):
        """ Return model predictions between 0 and 1.  """
        if state.shape == self.input_shape:
            return np.random.dirichlet(np.ones((self.output_shape,)), size=1)
        return np.random.dirichlet(np.ones((self.output_shape,)), size=state.shape[0])

    def compute_action(self, state):
        if state.shape == self.input_shape:
            return np.random.randint(0, self.output_shape)
        return np.random.randint(0, self.output_shape, (state.shape[0]))

    def update(self, *args, **kwargs):
        pass

    def finalize_episode(self, *args, **kwargs):
        return dict()
