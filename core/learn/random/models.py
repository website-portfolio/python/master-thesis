import numpy as np

from core.interface import Models


class RandomModel(Models):
    def __init__(self, input_shape, output_shape):
        self.input_shape = input_shape
        self.output_shape = output_shape

    def get_action_probabilities(self, state):
        """ Return model predictions between 0 and 1.  """
        if state.shape == self.input_shape:
            return np.random.dirichlet(np.ones((self.output_shape,)), size=1)
        return np.random.dirichlet(np.ones((self.output_shape,)), size=state.shape[0], )

    def get_best_action(self, state):
        """ return the best action for the provided state.  """
        if state.shape == self.input_shape:
            return np.random.randint(0, self.output_shape)
        return np.random.randint(0, self.output_shape, (state.shape[0]))

    def update(self, *args, **kwargs):
        """ Called when the model can train.  """
        pass

    def save(self, save_folder):
        """ Perform the saving of a model based on a provided folder.  """
        pass

    def load(self, load_folder):
        """ Perform the loading of a model based on a provided folder.  """
        pass
