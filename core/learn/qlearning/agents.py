import os
from typing import Union

import gym
import numpy as np

from core import utilities
from core.interface import Agent, Models
from core.memory.replay_buffer import ReplayBuffer
from core.wrappers.epsilon import EpsilonWrapper


class EpsilonGreedyAgent(Agent):
    def __init__(
            self,
            env: Union[gym.Env, EpsilonWrapper],
            model: Models,
            gamma: float = 0.99,
            batch_size: int = 32,
            replay_capacity: int = 10_000,
            normalize=True,
            training_start: int = 50,  # start training after x number of steps
            training_interval: int = 4,  # train every x steps
            use_epsilon: bool = False,  # if true actions are taken randomly with epsilon probability
            save_folder: str = None,
            save_best: bool = False,
            save_interval: int = None,
    ):

        self.env = env
        self.network = model
        self.gamma = gamma
        self.batch_size = batch_size
        self.replay_buffer = ReplayBuffer(replay_capacity)

        self.normalize = normalize
        self.training_start = training_start
        self.training_interval = training_interval
        self.episode_losses = []

        self.input_shape = env.observation_space.shape
        self.n_actions = env.action_space.n

        self.steps = 0
        self.runs = 0
        self.return_ = 0
        self.best_return = -np.uint16(-1)

        self.use_epsilon = use_epsilon
        if self.use_epsilon and not hasattr(self.env, 'epsilon'):
            raise AttributeError(f"Env doesn't have epsilon, set `use_epsilon` to False, or apply `EpsilonWrapper`.")

        self.save_folder = save_folder
        self.save_best = save_best
        self.save_interval = save_interval

    def get_action_probabilities(self, state):
        """ Return model predictions between 0 and 1.  """
        state = utilities.normalize(state, normalize=self.normalize)
        if state.shape == self.input_shape:
            return self.network.get_action_probabilities(np.expand_dims(state, axis=0))
        return self.network.get_action_probabilities(state)

    def compute_action(self, state):
        """
        Given the state return the action following epsilon-greedy strategy
        :param
            state (array_like): state of the system
        :return(int): action
        """
        if self.network is None:
            print("The network is not set. Please, create with Agent(network=network) or use agent.load()")
            raise ValueError()

        # When state is missing the batch dimension add it
        if state.shape == self.env.observation_space.shape:
            state = np.expand_dims(state, axis=0)

        if self.use_epsilon and np.random.rand() < self.env.epsilon:
            return np.random.randint(self.n_actions)

        state = utilities.normalize(state, normalize=self.normalize)
        action = self.network.get_best_action(state)
        action = action.numpy().item()
        return action

    def update(self, current_state, action, reward, next_state, is_terminal):
        """
        Append the transition to a replay buffer and train the network if necessary
        :param current_state: (array_like)
        :param action: (int)
        :param reward: (float)
        :param next_state: (array_like)
        :param is_terminal: (float)
        :return: loss of the network
        """
        self.return_ = reward + self.gamma * self.return_
        self.replay_buffer.store((current_state, action, reward, next_state, is_terminal))

        loss = 0
        if self.training_start < self.steps and self.steps % self.training_interval == 0:
            sample_batch = self.replay_buffer.sample_batch(self.batch_size)
            loss = self.network.update(*sample_batch)
            self.episode_losses.append(loss)

        self.steps += 1
        return loss

    def finalize_episode(self):
        if self.save_best and self.return_ > self.best_return and self.runs > 200:
            self.best_return = self.return_
            self.network.save(os.path.join(self.save_folder, type(self).__name__, 'best'))

        if self.save_interval and self.runs % self.save_interval == 0:
            self.network.save(os.path.join(self.save_folder, type(self).__name__, f"run {self.runs}"))

        stats = {
            "runs": self.runs,
            "return": self.return_,
            "epsilon": self.env.epsilon if self.use_epsilon else 0,
            "loss": float(np.mean(self.episode_losses)) if self.episode_losses else 0,
        }

        self.runs += 1
        self.return_ = 0
        self.episode_losses = []
        return stats

    def save_model(self, save_folder):
        self.network.save(save_folder=save_folder)

    def load_model(self, load_folder):
        self.network.load(load_folder=load_folder)
