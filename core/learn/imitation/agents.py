from collections import deque
from copy import deepcopy

import numpy as np
import tqdm
import tqdm.keras
from tensorflow.python.keras.utils.np_utils import to_categorical

from core import utilities
from core.interface import Agent


class ImitationAgent(Agent):
    """ This agent takes actions until the number of to collect games has been reached.  """

    def __init__(
            self,
            env,
            model,
            nr_games,
            minimal_score,
            progressbar=False,
            trained=False,
            action_func=None,
            ipython=False,
            **kwargs
    ):
        self.env = env
        self.network = model

        self.nr_games = nr_games
        self.minimal_score = minimal_score
        self.progressbar = progressbar
        self.trained = trained
        self.action_func = action_func
        self.ipython = ipython
        self.kwargs = kwargs

        self.collected_states = deque(maxlen=nr_games)
        self.collected_actions = deque(maxlen=nr_games)
        self.collected_rewards = deque(maxlen=nr_games)
        self._progress_bar = None

        self._temp_states = []
        self._temp_actions = []
        self._temp_rewards = []

    @property
    def done_collecting(self):
        return len(self.collected_rewards) == self.nr_games

    @property
    def training_data(self):
        if self.collected_states is None or self.collected_actions is None:
            raise ValueError(f"No training data available, run `collect_data` first.")

        # Flatten the data, since we are not interested in episodic behaviour.
        states = np.stack([state for game in self.collected_states for state in game], axis=0)
        actions = np.stack([action for game in self.collected_actions for action in game], axis=0)
        actions = to_categorical(actions, self.env.action_space.n)

        return states, actions

    def get_action_probabilities(self, state):
        """ Return the action probabilities for a specific state.  """
        if state.shape == self.env.observation_space.shape:
            return self.network.get_action_probabilities(np.expand_dims(state, axis=0))
        return self.network.get_action_probabilities(state)

    def compute_action(self, state):
        """ Return the action for the environment.  """
        if not self.trained:
            if self.action_func is not None:
                return self.action_func(state)
            return self.env.action_space.sample()

        if state.shape == self.env.observation_space.shape:
            return self.network.get_best_action(np.expand_dims(state, axis=0)).numpy().item()
        return self.network.get_best_action(state).numpy()

    def update(self, current_state, action, reward, next_state, done):
        """
            Append the transition to the temporary training data.

            :param current_state: (array_like)
            :param action: (int)
            :param reward: (float)
            :param next_state: (array_like)
            :param done: (float)
        """
        if not self.trained:
            self._temp_states.append(deepcopy(current_state))
            self._temp_actions.append(action)
            self._temp_rewards.append(reward)

    def finalize_episode(self, **kwargs) -> dict:
        """ Return the statistics of the agent.  """
        if self.trained:
            return dict()

        if self.progressbar and self._progress_bar is None:
            self._progress_bar = utilities.get_progressbar(
                    runs=self.nr_games,
                    plot_stats=None,
                    ipython=self.ipython,
                    desc='\tCollecting games'.ljust(20),
                    leave=True
            )
            self._progress_bar.n += 1

        if sum(self._temp_rewards) >= self.minimal_score:
            self.collected_states.append(self._temp_states[:])
            self.collected_actions.append(self._temp_actions[:])
            self.collected_rewards.append(self._temp_rewards[:])

            if self.progressbar and not self.done_collecting:
                self._progress_bar.set_postfix({'last score': sum(self._temp_rewards)})
                self._progress_bar.update(1)

        if self.progressbar and self.done_collecting:
            self._progress_bar.close()

        self._temp_states = []
        self._temp_actions = []
        self._temp_rewards = []
        return dict()

    def fit(self, epochs=20, callbacks=None, **kwargs):
        """ Fitting the training data to the model.  """
        states, actions = self.training_data

        if callbacks is None:
            verbose = kwargs.get('verbose', 0)
            callbacks = [tqdm.keras.TqdmCallback(epochs, position=0, verbose=verbose, desc='\tTraining'.ljust(20))]
            kwargs['verbose'] = 0  # disable tf verbosity, when using TqdmCallback

        history = self.network.fit(states, actions, epochs=epochs, callbacks=callbacks, **kwargs)
        self.trained = True
        return history

    def save_model(self, save_folder):
        self.network.save(save_folder=save_folder)

    def load_model(self, load_folder):
        self.network.load(load_folder=load_folder)
        self.trained = True
