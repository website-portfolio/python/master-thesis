import logging
import os

import tensorflow as tf
from tensorflow.keras.layers import Dense
from tensorflow.keras.models import Model
from tensorflow.python.keras.layers import Conv2D, Flatten, Reshape
from tensorflow.python.keras.models import load_model

from core.interface import Models


class NeuralNetwork(Models):
    def __init__(
            self,
            input_shape,
            output_shape,
            learning_rate=1e-4,
            use_cnn=True,
            cnn_number_of_maps=(32, 64, 64),
            cnn_kernel_size=(8, 4, 3),
            cnn_kernel_stride=(4, 2, 1),
            mlp_n_hidden=(32, 64, 64),
            mlp_activation_f="tanh",
            mlp_value_n_hidden=(32, 32, 64),
            mlp_value_activation_f="tanh",
            output_activation_f='softmax',

    ):
        self.settings = locals().copy()
        self.input_shape = input_shape
        self.output_shape = output_shape
        self.use_cnn = use_cnn

        # use these instead of mlp layers, if use cnn is True
        self.cnn_number_of_maps = cnn_number_of_maps
        self.cnn_kernel_size = cnn_kernel_size
        self.cnn_kernel_stride = cnn_kernel_stride

        # use these instead of cnn layers, if use cnn is False
        self.mlp_n_hidden = mlp_n_hidden
        self.mlp_activation_f = mlp_activation_f

        # use these after cnn or hidden layers
        self.mlp_value_n_hidden = mlp_value_n_hidden
        self.mlp_value_activation_f = mlp_value_activation_f
        self.output_activation_f = output_activation_f

        self.online_network = self.create_model("NeuralNetwork")

        self.optimizer = tf.keras.optimizers.Adam(learning_rate=learning_rate)
        self.loss_layer = tf.keras.losses.Huber()
        self.online_network.compile(optimizer=self.optimizer, loss=self.loss_layer, metrics=['accuracy'])

    def create_model(self, model_name):
        input_obs = tf.keras.Input(shape=self.input_shape, batch_size=None, name="input", dtype=tf.float32)
        x = self.create_input_embedding(input_obs)

        for i, n_dim in enumerate(self.mlp_value_n_hidden):
            x = Dense(n_dim, activation=self.mlp_value_activation_f, name=f"dense_value_{i}")(x)

        output = Dense(self.output_shape, activation=self.output_activation_f, name="output")(x)
        network = Model(inputs=[input_obs], outputs=[output], name=model_name)
        return network

    def get_best_action(self, state):
        return tf.argmax(self.online_network(state), axis=1)

    @tf.function
    def get_best_action_value(self, state):
        return tf.reduce_max(self.online_network(state), axis=1)

    @tf.function
    def get_action_probabilities(self, state):
        return tf.nn.softmax(self.online_network(state))

    def fit(self, x, y, *args, **kwargs):
        return self.online_network.fit(x, y, *args, **kwargs)

    def update(self, *args, **kwargs):
        pass

    def create_input_embedding(self, input_tensor):
        x = input_tensor

        if self.use_cnn:
            # Handle missing dimension in states.
            if len(self.input_shape) == 2:
                logging.getLogger('rl-base').warning(f"Reshaped input to get 3D dimensions for the Conv2D layers.")
                x = Reshape(target_shape=(*self.input_shape, 1))(x)

            cnn_settings = zip(self.cnn_number_of_maps, self.cnn_kernel_size, self.cnn_kernel_stride)
            for i, (n_filters, kernel_size, strides) in enumerate(cnn_settings):
                x = Conv2D(n_filters, kernel_size, strides, padding='valid', activation='relu', name=f"conv_{i}")(x)
            x = Flatten()(x)

        else:
            # Handle non flattened states.
            if len(self.input_shape) > 1:
                logging.getLogger('rl-base').warning(f"Flattened input for hidden layers (wasn't 1D).")
                x = Flatten()(x)

            for i, n_dim in enumerate(self.mlp_n_hidden):
                x = Dense(n_dim, activation=self.mlp_activation_f, name=f'dense_hidden_{i}')(x)
        return x

    def save(self, save_folder):
        full_path = os.path.join(save_folder, 'weights.tf')
        self.online_network.save(full_path)

    def load(self, load_folder):
        full_path = os.path.join(load_folder, 'weights.tf')
        self.online_network = load_model(full_path)
        return self.online_network
