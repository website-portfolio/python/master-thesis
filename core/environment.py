"""
    Generate an environment with all kinds of wrapper options.
"""

from core import CatchEnv
from core.envs.utils.constants import Colors
from core.envs.utils.obscure import obscure
from core.wrappers import EpsilonWrapper, Augmentation
from core.wrappers.attention import *
from core.wrappers.frame_stack import FrameStack


def create_env(
        display='gray',
        reset='random',
        inverse=False,
        obscure_pattern=None,
        obscure_color=None,
        attention: str = None,
        attention_mask: bool = False,
        a_min=0.,
        a_max=1.,
        fwhm=3,
        cluster=False,
        frame_stack: int = None,
        training=False,
        aug_kwargs=None,
        **kwargs,
):
    """ Create an environment.  """
    settings = {k: v for k, v in locals().items() if k != 'kwargs'}
    settings.update(**kwargs)

    env = CatchEnv(display=display, reset=reset, **kwargs)

    if inverse:
        env = add_inversion(env, display=display)

    if training and aug_kwargs:
        env = Augmentation(env, **aug_kwargs)

    if obscure_pattern is not None:
        env = obscure(env, pattern=obscure_pattern, color=obscure_color)

    if attention is not None:
        # Apply attention on the `player` and `goal` colors or positions.
        env = add_attention(env, attention=attention, a_min=a_min, a_max=a_max, fwhm=fwhm, cluster=cluster)

    if attention_mask:
        # Return the attention mask instead of the image multiplied with the attention
        env = AttentionMaskWrapper(env)

    if frame_stack:
        env = FrameStack(env, num_stack=frame_stack)

    if training:
        env = EpsilonWrapper(env, epsilon_start=1.0, epsilon_end=0.02, epsilon_decay=5e-4)

    return env, settings


def add_inversion(env, display):
    if display == 'binary':
        env.background.color = 1 - env.background.color
        env.goal.color = 1 - env.goal.color
        env.player.color = 1 - env.player.color

    elif display == 'gray':
        env.background.color = 255 - env.background.color
        env.goal.color = 255 - env.goal.color
        env.player.color = 255 - env.player.color

    elif display == 'color':
        env.background.color = Colors(*list(map(lambda x: 255 - x, env.background.color)))
        env.goal.color = Colors(*list(map(lambda x: 255 - x, env.goal.color)))
        env.player.color = Colors(*list(map(lambda x: 255 - x, env.player.color)))

    else:
        raise ValueError(f"No inverse defined for display {display!r}")

    return env


def add_attention(env, attention, a_min, a_max, fwhm, cluster):
    if attention == 'color':
        return AttentionColorWrapper(
                env,
                a_min=a_min,
                a_max=a_max,
                fwhm=fwhm,
                colors=[env.goal.color, env.player.color],
                cluster=cluster
        )

    if attention == 'position':
        return AttentionPositionWrapper(
                env,
                a_min=a_min,
                a_max=a_max,
                fwhm=fwhm,
                sprites=[env.goal, env.player]
        )

    if attention == 'double':
        return DoubleAttentionWrapper(
                env=env,
                colors=[env.goal.color, env.player.color],
                sprites=[env.goal, env.player],
                a_min=a_min,
                a_max=a_max,
                fwhm=fwhm,
                cluster=cluster
        )

    raise ValueError(f"Unknown attention type {attention!r}")
