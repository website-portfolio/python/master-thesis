import cv2
import gym
import numpy as np


class RecolorWrapper(gym.Wrapper):
    """
        Locates and replaces all specific values of an image with the new respective colors.
        The image type is deduced from update color values, if single values are given, the
        values are replaced on a pixel by pixel bases, if a color tuple is given the colors
        are replaced on the basis of the whole axis needing to have that color tuple.

        :param update_colors: (Dict[Tuple[int, ...], Tuple[int, ...]])
            With key the color that has to be replaced by the value.
        :param axis: (int)
            The axis on which the color changes have to occur, default axis is the last axis (-1).
        :param dtype: (Union[np.bool, np.integer])
            The type of the color representation (this can speed up numpy matching).
    """

    def __init__(self, env, update_colors: dict, axis=-1, dtype=np.uint8):
        if env is not None: super().__init__(env)

        self.values_old = np.array(list(update_colors.keys()), dtype=dtype)
        self.values_new = np.array(list(update_colors.values()), dtype=dtype)

        self.process_color_tuples = len(self.values_old.shape) > 1
        self.axis = axis
        self.dtype = dtype
        self.image = None

    def update_color_settings(self, update_colors: dict):
        """ Change the current colors to new colors.  """
        self.values_old = np.array(list(update_colors.keys()), dtype=self.dtype)
        self.values_new = np.array(list(update_colors.values()), dtype=self.dtype)
        self.process_color_tuples = len(self.values_old.shape) > 1

    def step(self, action):
        img, reward, done, info = self.env.step(action)
        self.image = self.process(img)
        return self.image, reward, done, info

    def reset(self):
        img = self.env.reset()
        self.image = self.process(img)
        return self.image

    def render(self, mode='human', size=None, **kwargs):
        """ Overwriting gym render to display new game colors.  """
        if mode != 'recolor' or self.image is None:
            return self.env.render(mode, **kwargs)

        if size:
            image = cv2.resize(self.image, size, interpolation=cv2.INTER_NEAREST_EXACT)
            cv2.imshow('Recolored observation', cv2.cvtColor(image, cv2.COLOR_BGR2RGB))
        else:
            cv2.imshow('Recolored observation', cv2.cvtColor(self.image, cv2.COLOR_BGR2RGB))
        
        cv2.waitKey(1)
        return self.image

    def close(self):
        cv2.destroyWindow('Recolored observation')
        self.env.close()

    def process(self, img: np.array):
        """ Update an input img with the color change."""
        if self.process_color_tuples:
            return self._process_ndarray(img)
        return self._process_array(img)

    def _process_ndarray(self, img):
        """ Change the color of a whole axis, instead of every individual value.  """
        out = img.copy()
        for color_old, color_new in zip(self.values_old, self.values_new):
            out[np.array(img == color_old).all(axis=self.axis)] = color_new
        return out.astype(np.uint8)

    def _process_array(self, img):
        """ Change the color of every individual pixel in the image.  """
        out = img.copy()
        for color_old, color_new in zip(self.values_old, self.values_new):
            out[img == color_old] = color_new
        return out.astype(np.uint8)
