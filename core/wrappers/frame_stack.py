from collections import deque

import gym
import numpy as np


class FrameStack(gym.Wrapper):
    """ Converter for MultiEnv generated images.  """

    def __init__(self, env, num_stack=4):
        super().__init__(env)
        self._num_stack = num_stack
        self._frames = deque([], maxlen=self._num_stack)
        self.observation_space = self._set_observation_space(num_stack=self._num_stack)

    def _set_observation_space(self, num_stack):
        if len(self.observation_space.low.shape) == 2:
            low = np.repeat(self.observation_space.low[..., np.newaxis], num_stack, axis=-1)
            high = np.repeat(self.observation_space.high[..., np.newaxis], num_stack, axis=-1)
        else:
            low = np.repeat(self.observation_space.low, num_stack, axis=-1)
            high = np.repeat(self.observation_space.high, num_stack, axis=-1)
        return gym.spaces.Box(low=low, high=high, dtype=self.observation_space.dtype)

    def reset(self):
        img = self.env.reset()
        self._create_stacks(img)
        return self._get_images()

    def step(self, actions):
        images, reward, done, info = self.env.step(actions)
        self._frames.append(images.copy())
        return self._get_images(), reward, done, info

    def _create_stacks(self, images):
        for _ in range(self._num_stack):
            self._frames.append(images)

    def _get_images(self):
        return np.array(LazyFrames(list(self._frames)))


class LazyFrames(object):
    def __init__(self, frames):
        """
            This object ensures that common frames between the observations are only stored once.
            It exists purely to optimize memory usage which can be huge for DQN's 1M frames replay
            buffers.

            This object should only be converted to numpy array before being passed to the model.
            You'd not believe how complex the previous solution was.
        """
        self._frames = frames
        self._out = None

    def _force(self):
        if self._out is None:
            self._out = np.stack(self._frames, axis=0)
            self._out = np.moveaxis(self._out, source=0, destination=-1)  # Move frame stack to last axis.
            self._frames = None

        if len(self._out.shape) == 4:
            self._out = np.reshape(self._out, (*self._out.shape[:2], -1))

        return self._out

    def __array__(self, dtype=None):
        out = self._force()
        if dtype is not None:
            out = out.astype(dtype)
        return out

    def __len__(self):
        return len(self._force())

    def __getitem__(self, i):
        return self._force()[i]

    def count(self):
        frames = self._force()
        return frames.shape[frames.ndim - 1]

    def frame(self, i):
        return self._force()[..., i]


if __name__ == '__main__':
    env = gym.make('Pong-v0')
    env = FrameStack(env, num_stack=4)

    print(env.observation_space.shape)
    print(np.array(env.reset()).shape)
