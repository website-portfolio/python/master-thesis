from argparse import Namespace
from collections import namedtuple
from typing import Tuple, List, Union

import cv2
import gym
import numpy as np


class Augmentation(gym.Wrapper):
    """
        Applying data augmentation from the paper Lasking M et al., 2020

        See there repository for the original code:
            - https://github.com/MishaLaskin/rad/blob/master/data_augs.py

        Notes:
            They are using (Batch, Channel, Height, Width) input images,
            While here (Height, Width, Channel) is assumed (no batch dimensions).

            All implementations have been created using only numpy,
            such that there is no dependency on pytorch or tensorflow.
    """

    _cutout_region = namedtuple('cutout', ('min', 'max'))

    def __init__(
            self,
            env,
            crop: int = None,
            gray_p: float = 0,
            flip_p: float = 0,
            rotation_p: float = 0,
            cutout: Tuple[int, int] = None,
            cutout_value: int = 0,
            cutout_color: Tuple[int, int] = None,
            color_jitter: dict = None,
            translate: dict = None,
            order: Union[List, Tuple] = None
    ):
        """

        :param env: Union[gym.Env, CatchEnv]
        :param crop: (int) width and height of the cropped image.
        :param gray_p: (float) probability [0, 1] of an image stack being transferred to gray scale.
        :param flip_p: (float) probability [0, 1] of an image being flipped.
        :param rotation_p: (float) probability [0, 1] of an image being rotated.
        :param cutout: (Tuple[int, int]) min and maximum width and height of the cutout rectangle.
        :param cutout_color: (int) color value of the cutout region, by default black.
        :param color_jitter: (Tuple[int, int]) min and maximum width and height of the cutout rectangle.
        :param translate: (Dict[str, Union[float, Tuple[int, int]]]) color jitter ranges.
        :param order: (Union[List, Tuple]) Define the order of the augmentations.
        """
        super().__init__(env)
        self.crop = crop
        self.gray_probability = gray_p
        self.flip_probability = flip_p
        self.rotation_probability = rotation_p
        self.cutout_region = self._cutout_region(*cutout)
        self.cutout_value = cutout_value or 0
        self.cutout_color_region = self._cutout_region(*cutout_color)
        self.color_jitter = Namespace(**(color_jitter or dict(p=0)))
        self.translate = Namespace(**(translate or dict(p=0)))

        self.execution = dict(
                crop=self._crop,
                gray=self._gray,
                flip=self._flip,
                rotation=self._rotation,
                cutout=self._cutout,
                cutout_color=self._cutout_color,
                color_jitter=self._color_jitter,
                translate=self._translate,
        )

        self._order = self._verify_input(order)
        self.execution = {k: self.execution[k] for k in self._order}
        self.original = self.env.observation_space.low
        self.observation_space = self._set_observation_space()
        self.augmentation = self.env.observation_space.low

    @property
    def order(self):
        return self.execution.keys()

    def _verify_input(self, order):
        """ Verify that all input augmentation are valid.  """
        order = list(map(str.lower, order))
        assert all(aug in self.execution for aug in order), \
            f"Not all augmentations are available." \
            f"\nInvalid: {list(aug for aug in order if aug not in self.order)}" \
            f"\nAvailable: {list(self.execution.keys())}"
        return order

    def _set_observation_space(self):
        low = self.observation_space.low
        high = self.observation_space.low

        if 'crop' in self.order:
            height, width = low.shape[:2]
            low = low[:min(self.crop, height), :min(self.crop, width), ...]
            high = low[:min(self.crop, height), :min(self.crop, width), ...]

        return gym.spaces.Box(low=low, high=high, dtype=self.observation_space.dtype)

    def step(self, action):
        self.original, reward, done, info = self.env.step(action)
        self.agumentation = self.process(self.original)
        return self.agumentation, reward, done, info

    def reset(self, **kwargs):
        self.original = self.env.reset(**kwargs)
        self.agumentation = self.process(self.original.copy())
        return self.agumentation

    def render(self, mode='human', **kwargs):
        if mode != 'augmentation':
            return self.env.render(mode, **kwargs)

        cv2.imshow(f"Augmentation: {self.order}", self.augmentation)
        cv2.waitKey(1)
        return self.augmentation

    def process(self, state):
        for aug, func in self.execution.items():
            state = func(state)
        return state.copy()

    def _crop(self, state):
        """ Reduce the dimensions of the image. """
        height, width = state.shape[:2]
        crop_max = height - self.crop + 1
        crop_width, crop_height = np.random.randint(0, crop_max, 2)
        return state[crop_height:crop_height + self.crop, crop_width:crop_width + self.crop, ...]

    def _gray(self, state):
        """ Convert RGB image to Gray, skip all other images.  """
        if np.random.rand() >= self.gray_probability:
            return state

        if len(state.shape) == 2:
            return state

        if len(state.shape) == 3:
            state = np.einsum('ijk,k->ij', state, [0.2989, 0.587, 0.114]).astype(np.uint8)
            return np.repeat(state[..., np.newaxis], 3, axis=-1)

        if len(state.shape) == 4:
            state = np.einsum('ijkl,l->ijl', state, [0.2989, 0.587, 0.114]).astype(np.uint8)
            return np.repeat(state[..., np.newaxis], 4, axis=-1)

        raise ValueError(f"Unknown number of dimension {len(state.shape)!r}")

    def _flip(self, state: np.ndarray):
        """ Randomly flip an image, over horizontal, vertical or both axis.  """
        if np.random.rand() >= self.flip_probability:
            return state
        if np.random.rand() >= .66:
            return np.flip(state, (0, 1))
        return np.flip(state, np.random.choice([0, 1]))

    def _rotation(self, state):
        """ Randomly rotate an image.  """
        if np.random.rand() > self.flip_probability:
            return state
        return np.rot90(state, k=np.random.randint(-4, 4))

    def _cutout(self, state, color=None):
        """ Cover a part of the screen with a random rectangle.  """
        height, width = state.shape[:2]
        x = np.random.randint(0, width)
        y = np.random.randint(0, height)
        height, width = np.random.randint(*self.cutout_region, 2)
        color = color if color is not None else self.cutout_value
        state[y: y + height, x: x + width, ...] = color
        return state

    def _cutout_color(self, state):
        """ Cover a part of the screen with a randomly colored rectangle.  """
        color = np.random.randint(0, 255, state.shape[2:] or 1)
        return self._cutout(state, color=color)

    def _color_jitter(self, state):
        """ Randomly change the HSV value of the image.  """
        if np.random.rand() >= self.color_jitter.p:
            return state

        image = cv2.cvtColor(state, cv2.COLOR_RGB2HSV)
        image[..., 0] += np.random.randint(*self.color_jitter.hue)
        image[..., 1] += np.random.randint(*self.color_jitter.saturation)
        image[..., 2] += np.random.randint(*self.color_jitter.value)
        image = cv2.cvtColor(image, cv2.COLOR_HSV2RGB)
        return image

    def _translate(self, state):
        """ Translates the image randomly with dx, dy. """
        if np.random.rand() >= self.translate.p:
            return state

        height, width = state.shape[:2]
        offset_x = np.random.randint(*self.translate.dx)
        offset_y = np.random.randint(*self.translate.dy)

        translated = self._translate_image(state, width, height, offset_x, offset_y)
        return translated

    def _translate_image(self, state, width, height, offset_x, offset_y, ):
        """ Translate an image.  """
        translated = np.roll(state, offset_x, axis=1)
        translated = np.roll(translated, -offset_y, axis=0)
        x = slice(offset_x, width) if offset_x < 0 else slice(0, offset_x)
        y = slice(-offset_y, height) if offset_y > 0 else slice(0, -offset_y)
        translated[:, x] = 0
        translated[y, :] = 0
        return translated

    def close(self):
        cv2.destroyWindow(f"Augmentation: {self.order}")
        self.env.close()
