import cv2
import gym
import numpy as np

from core import utilities
from core.envs.catch import CatchEnv
from core.interface import Agent
from core.vis.quiver import QuiverPlot


class QuiverWrapper(gym.ObservationWrapper):
    """ Return the normal observation and the quiver plot for the environment.  """
    supported_environments = ['Catch']
    return_image_modes = ['obs', 'rgb_array', 'quiver', 'both']

    def __init__(self, env, model, return_image='both'):
        super().__init__(env)
        self.verify(env, return_image)
        self.return_image = return_image

        self.plotter = QuiverPlot(env, model)
        self._last_observation = None

    def render(self, mode='human', **kwargs):
        if mode in ['quiver', 'rgb_array'] and self.return_image in ['quiver', 'both']:
            return self.create_quiver(self._last_observation)
        return self.env.render(mode=mode, **kwargs)

    def verify(self, env: CatchEnv, return_image):
        if not any(env.spec.id.startswith(id) for id in self.supported_environments):
            raise ValueError(f"Unsupported environment {env.spec.id!r}, valid values {self.supported_environments}")
        if not return_image in self.return_image_modes:
            raise ValueError(f"Unsupported display {return_image!r}, valid values {self.return_image_modes}.")

    def _generate_quiver_plot(self, obs):
        width, height = obs.shape[:2]
        data = self.plotter.all_possible_predictions(obs, self.env.goal)
        quiver_data = self.plotter.generate_quiver_data(data)

        # Skip over the first sprite, since this is the background.
        patches = [self.plotter.generate_sprite_patch(sprite) for sprite in list(self.env.sprites.values())[1:]]
        fig = self.plotter.plot(quiver_data, patches=patches, limits=((-1, width), (-1, height + 1)), show=False)
        return fig

    def observation(self, observation):
        """ Returns the optionally changed observation.  """
        self._last_observation = observation.copy()
        if self.return_image == 'obs':
            return observation
        return self.create_quiver(observation)

    def create_quiver(self, observation):
        """ Generate a separate quiver image, or in combination with the original obs. """
        fig = self._generate_quiver_plot(observation)
        quiver_observation = utilities.fig_to_numpy(fig)
        quiver_observation = self.add_prediction_text(quiver_observation, observation)

        if self.return_image == 'quiver':
            return quiver_observation

        if self.return_image == 'both':
            observation = self.env.render(mode='rgb_array')
            return utilities.stack_images([observation, quiver_observation], columns=2)

        if self.return_image == 'rgb_array':
            return self.env.render(mode='rgb_array')

        raise ValueError(f"Unknown return mode: {self.return_image!r}")

    def add_prediction_text(self, quiver_obs, obs):
        """ Writes the prediction values and chosen action to the screen.  """
        if obs.shape == self.env.observation_space.shape:
            obs = np.expand_dims(obs, axis=0)

        if isinstance(self.plotter.model, Agent):
            action = np.array(self.plotter.model.compute_action(obs)).flatten()[0]
        else:
            action = np.array(self.plotter.model.get_best_action(obs)).flatten()[0]

        prediction = [f"{val:5.3f}" for val in self.plotter.model.get_action_probabilities(obs)[0]]
        text = f"{prediction}, {action} {dict((v, k) for k, v in self.env.action_meanings.items())[action]}"
        quiver_obs = cv2.putText(quiver_obs, text, (115, 60), cv2.FONT_HERSHEY_COMPLEX_SMALL, 2, (255, 0, 0), 2, 1)
        return quiver_obs


class QuiverWrapperAttention(QuiverWrapper):
    def _generate_quiver_plot(self, obs):
        width, height = obs.shape[:2]
        data = self.plotter.all_possible_predictions_attention(obs, self.env.goal)
        quiver_data = self.plotter.generate_quiver_data(data)

        # Skip over the first sprite, since this is the background.
        patches = [self.plotter.generate_sprite_patch(sprite) for sprite in list(self.env.sprites.values())[1:]]
        fig = self.plotter.plot(quiver_data, patches=patches, limits=((-1, width), (-1, height + 1)), show=False)
        return fig
