from typing import List

import cv2
import gym
import numpy as np
import scipy.cluster.hierarchy as h_cluster

from core import utilities
from core.attention import Gaussian
from core.envs.utils.sprite import Sprite


class AttentionColorWrapper(gym.Wrapper):
    def __init__(self, env, colors, a_min=0., a_max=1., fwhm=3, threshold=3, cluster=False):
        """
            Provide a gaussian attention focus on specific colors.
            Requires colors to be RGB encoded.

            :param env: gym.Env
                Should be an image based input, all other inputs might make
                less sense.
            :param colors: Iterable[int, ...]
                List of colors that have to be highlighted more.
            :param a_min: float
                Minimal attention value to be applied.
            :param a_max: float
                Maximum attention value to be applied.
            :param fwhm: int
                The higher the value the wider the range of attention is.
            :param threshold: int
                The threshold used for the clustering algorithm, it is adviced
                that the value is similar to the fwhm value.

        """
        super().__init__(env)
        self.metadata['render.modes'].append('attention')

        self.colors = list(set(colors))
        self.a_min = a_min
        self.a_max = a_max

        self.fwhm = fwhm
        self.threshold = threshold
        self.cluster = cluster

        self.colors_are_tuples = self._check_colors_are_tuples(env)

        self.gaussian = Gaussian(size=self.env.observation_space.shape[:2])
        self.dim = len(self.env.observation_space.shape[:2])
        self.original = None
        self.mask = None

    def __getattr__(self, item):
        return getattr(self.env, item)

    def _check_colors_are_tuples(self, env: gym.Env):
        img = env.observation_space.low
        if img.dtype != np.uint8:
            raise ValueError(f"Color Attention is only implemented for images of type `np.uint8`.")
        if all(isinstance(color, (list, tuple)) for color in self.colors):
            return True
        if all(isinstance(color, int) for color in self.colors):
            return False
        raise ValueError("Color types are not uniquely `list` or `int`.")

    def step(self, action):
        img, reward, done, info = self.env.step(action)
        img = self.process(img)
        return img, reward, done, info

    def reset(self):
        img = self.env.reset()
        img = self.process(img)
        return img

    def render(self, mode='human', delay=1, **kwargs):
        """ Overwriting gym render to display new attention colors.  """
        if self.mask is None or mode != 'attention':
            return self.env.render(mode, **kwargs)

        attention = np.array(self.mask * 255, dtype=np.uint8)
        attention_image = np.array(self.original * self.mask, dtype=np.uint8)
        result = utilities.stack_images(images=[self.original, attention, attention_image],
                                        columns=3, size=(300, 300))

        cv2.imshow('Attention Colors', cv2.cvtColor(result, cv2.COLOR_BGR2RGB))
        if cv2.waitKey(delay) & 0xFF == ord('q'):
            raise KeyboardInterrupt
        return result

    def close(self):
        cv2.destroyWindow('Attention Colors')
        self.env.close()

    def process(self, img: np.array):
        """ Update an input img with the color change."""
        self.original, self.mask = img.copy(), img
        self.mask = np.clip(self._process_ndarray(img), a_min=self.a_min, a_max=self.a_max)
        return np.array(self.original * self.mask, dtype=np.uint8)

    def _process_ndarray(self, img):
        """ Generate attention based on the color locations.  """

        # Locate all the colors.
        locations = self._locate_colors(img, self.colors_are_tuples)
        centers = np.vstack([np.vstack(indices).T for indices in locations])

        if not centers.size:
            return np.ones_like(self.original)

        centers = self._clean_centers(centers)
        if self.cluster:
            centers = self._cluster_centers(centers)

        distribution = self.gaussian.creates(self.dim, self.fwhm, centers)
        if len(self.observation_space.shape) == 3:
            return np.repeat(distribution[..., np.newaxis], self.observation_space.shape[-1], axis=-1)
        return distribution

    def _clean_centers(self, centers: np.ndarray):
        # TODO make these settings more general, right now specific for pong.
        if self.env.spec.id.startswith('Pong'):
            centers = centers[np.bitwise_and(33 < centers[:, 0], centers[:, 0] < 194)]
        return centers

    def _cluster_centers(self, centers):
        clusters = h_cluster.fclusterdata(centers, self.threshold, criterion='distance')
        centers = [np.average(centers[clusters == cluster], axis=0) for cluster in np.unique(clusters)]
        return centers

    def _locate_colors(self, img, colors_are_tuples):
        if colors_are_tuples:
            return [np.where((img == color).all(axis=-1)) for color in self.colors]
        return [np.where(img == color) for color in self.colors]


class AttentionPositionWrapper(AttentionColorWrapper):
    def __init__(self, env, sprites: List[Sprite], a_min=0., a_max=1., fwhm=3):
        super().__init__(env, colors=(), a_min=a_min, a_max=a_max, fwhm=fwhm)
        self.env = env
        self.fwhm = fwhm
        self._sprites = sprites

    def _process_ndarray(self, img):
        centers = []
        for sprite in self._sprites:
            grid = np.mgrid[sprite.y:sprite.y + sprite.height, sprite.x:sprite.x + sprite.width]
            loc = grid.reshape(self.dim, -1).T
            centers.append(loc)

        centers = np.vstack(centers)
        if self.dim == 3:
            centers = np.c_[centers, np.ones(len(centers))]

        if not centers.size:
            return np.ones_like(self.original)

        if self.cluster:
            centers = self._cluster_centers(centers)

        distribution = self.gaussian.creates(self.dim, self.fwhm, centers)
        if len(self.observation_space.shape) == 3:
            return np.repeat(distribution[..., np.newaxis], self.observation_space.shape[-1], axis=-1)
        return distribution


class DoubleAttentionWrapper(AttentionColorWrapper):
    def __init__(self, env, colors, sprites, a_min=0., a_max=1., fwhm=3, threshold=3, cluster=False):
        """
            Provide a gaussian attention focus on specific colors.
            Requires colors to be RGB encoded.

            :param env: gym.Env
                Should be an image based input, all other inputs might make
                less sense.
            :param colors: Iterable[int, ...]
                List of colors that have to be highlighted more.
            :param fwhm: int
                The higher the value the wider the range of attention is.
            :param threshold: int
                The threshold used for the clustering algorithm, it is adviced
                that the value is similar to the fwhm value.

        """
        super().__init__(env, colors, a_min=a_min, a_max=a_max, fwhm=fwhm, threshold=threshold, cluster=cluster)
        self._sprites = sprites

    def render(self, mode='human', delay=1, **kwargs):
        """ Overwriting gym render to display new attention colors.  """
        if self.mask is None or mode != 'attention':
            return self.env.render(mode, **kwargs)

        attention_colors = np.array(self._mask_colors * 255, dtype=np.uint8)
        attention_positions = np.array(self._mask_position * 255, dtype=np.uint8)
        attention_combined = np.array(self.mask * 255, dtype=np.uint8)
        attention_image = np.array(self.original * self.mask, dtype=np.uint8)

        result = utilities.stack_images(
                images=[attention_image, self.original, attention_combined,
                        attention_colors, attention_positions],
                columns=3, size=(300, 300))

        cv2.imshow('Attention Colors', cv2.cvtColor(result, cv2.COLOR_BGR2RGB))
        if cv2.waitKey(delay) & 0xFF == ord('q'):
            raise KeyboardInterrupt
        return result

    def process(self, img: np.array):
        """ Update an input img with the color change."""
        self.original, self.mask = img.copy(), img
        self._mask_colors = self._process_colors(img)
        self._mask_position = self._process_location()
        self.mask = np.maximum(self._mask_colors, self._mask_position)
        self.mask = np.clip(self.mask, a_min=self.a_min, a_max=self.a_max)
        return np.array(self.original * self.mask, dtype=np.uint8)

    def _process_colors(self, img):
        """ Generate attention based on the color locations.  """

        # Locate all the colors.
        locations = self._locate_colors(img, self.colors_are_tuples)
        centers = np.vstack([np.vstack(indices).T for indices in locations])

        if not centers.size:
            return np.ones_like(self.original)

        centers = self._clean_centers(centers)
        if self.cluster:
            centers = self._cluster_centers(centers)

        distribution = self.gaussian.creates(self.dim, self.fwhm, centers)
        if len(self.observation_space.shape) == 3:
            return np.repeat(distribution[..., np.newaxis], self.observation_space.shape[-1], axis=-1)
        return distribution

    def _process_location(self):
        """ Generate attention based on sprite locations.  """
        centers = []
        for sprite in self._sprites:
            grid = np.mgrid[sprite.y:sprite.y + sprite.height, sprite.x:sprite.x + sprite.width]
            loc = grid.reshape(self.dim, -1).T
            centers.append(loc)

        centers = np.vstack(centers)
        if self.dim == 3:
            centers = np.c_[centers, np.ones(len(centers))]

        if not centers.size:
            return np.ones_like(self.original)

        if self.cluster:
            centers = self._cluster_centers(centers)

        distribution = self.gaussian.creates(self.dim, self.fwhm, centers)
        if len(self.observation_space.shape) == 3:
            return np.repeat(distribution[..., np.newaxis], self.observation_space.shape[-1], axis=-1)
        return distribution


class AttentionMaskWrapper(gym.Wrapper):
    def __init__(self, env):
        super().__init__(env)

        if not hasattr(env, 'mask'):
            raise AttributeError(f"Environment needs to be pre wrapped with a AttentionWrapper")

    def step(self, action):
        img, reward, done, info = self.env.step(action)
        self.process(img)
        return self.mask, reward, done, info

    def reset(self):
        img = self.env.reset()
        self.process(img)
        return self.mask
