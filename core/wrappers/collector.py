from collections import deque
from copy import deepcopy

import gym
import tqdm


class GamesCollector(gym.Wrapper):
    """ Game collections wrapper to store good games for imitation learning.  """

    def __init__(self, env, nr_games, minimal_score, progressbar=False):
        super().__init__(env)
        self.nr_games = nr_games
        self.minimal_score = minimal_score
        self.progressbar = progressbar

        self.collected_states = deque(maxlen=nr_games)
        self.collected_actions = deque(maxlen=nr_games)
        self.collected_rewards = deque(maxlen=nr_games)

        self._temp_states = []
        self._temp_actions = []
        self._temp_rewards = []

        self._progress_bar = tqdm.trange(nr_games, desc="\tCollecting games".ljust(20), position=0, leave=True)

    @property
    def done_collecting(self):
        return len(self.collected_rewards) == self.nr_games

    def step(self, action):
        obs, reward, done, info = self.env.step(action)

        if len(self.collected_rewards) < self.nr_games:
            self._store_data(obs, reward, done, action)
        return obs, reward, done, info

    def _store_data(self, obs, reward, done, action):
        self._temp_states.append(deepcopy(obs))
        self._temp_actions.append(deepcopy(action))
        self._temp_rewards.append(deepcopy(reward))

        if done and sum(self._temp_rewards) >= self.minimal_score:
            self.collected_states.append(self._temp_states[:])
            self.collected_actions.append(self._temp_actions[:])
            self.collected_rewards.append(self._temp_rewards[:])

            if self.progressbar:
                self._progress_bar.set_postfix({'last score': sum(self._temp_rewards)})
                self._progress_bar.update(1)

                if self._progress_bar.n == self.nr_games:
                    self._progress_bar.close()
                    self.progressbar = False

    def reset(self):
        self._temp_states = []
        self._temp_actions = []
        self._temp_rewards = []

        obs = self.env.reset()
        return obs

    def close(self):
        self._progress_bar.close()
        self.env.close()
