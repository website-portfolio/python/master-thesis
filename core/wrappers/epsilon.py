import math

import gym
import matplotlib.pyplot as plt
import numpy as np


class EpsilonWrapper(gym.Wrapper):
    """ Wrap an environment with an annealing epsilon value.  """
    valid_interpolation = ['exponential', 'linear']

    def __init__(self, env, epsilon_start=1., epsilon_end=0.02, epsilon_decay=5e-6, interpolation='exponential'):
        super().__init__(env)
        self.epsilon_start = epsilon_start
        self.epsilon_end = epsilon_end
        self.epsilon_diff = self.epsilon_start - self.epsilon_end
        self.epsilon_decay = epsilon_decay
        self.interpolation = interpolation

        self.epsilon = None
        self.time_steps = None
        self.reset_epsilon()

    def step(self, action):
        self.time_steps += 1
        self._update(self.time_steps)
        return self.env.step(action)

    def reset(self, **kwargs):
        self.time_steps += 1
        self._update(self.time_steps)
        return self.env.reset(**kwargs)

    def reset_epsilon(self):
        self.epsilon = self.epsilon_start
        self.time_steps = 0

    def _update(self, time_step):
        if self.interpolation == 'linear':
            return self._update_epsilon_linear(time_step)
        if self.interpolation == 'exponential':
            return self._update_epsilon_exponential(time_step)
        raise ValueError(f'{self.interpolation!r} not implemented, valid interpolations are {self.valid_interpolation}')

    def _update_epsilon_exponential(self, time_step):
        self.epsilon = self.epsilon_end + self.epsilon_diff * math.exp(-self.epsilon_decay * time_step)
        return self.epsilon

    def _update_epsilon_linear(self, time_step=1):
        self.epsilon = max(self.epsilon_end, self.epsilon_start - self.epsilon_decay * time_step)
        return self.epsilon

    def epsilon_plot(self, step_start=0, step_end=20_000, step_size=100, block=False):
        fig, ax = plt.subplots()
        ax.set_ylabel('epsilon')
        ax.set_xlabel('steps')
        ax.set_title(f"{self.interpolation.capitalize()} epsilon decay")

        time_steps = np.linspace(step_start, step_end, step_size)
        epsilon_values = [self._update(time_step) for time_step in time_steps]
        ax.plot(time_steps, epsilon_values)

        plt.show(block=block)


if __name__ == '__main__':
    env = gym.make('CartPole-v0')
    env = EpsilonWrapper(env=env, epsilon_start=1, epsilon_end=0.02, epsilon_decay=5e-4, interpolation='exponential')
    env.epsilon_plot(step_end=1_000 * 20, block=True)
