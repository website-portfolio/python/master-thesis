from copy import deepcopy

from core import CatchEnv
from core.envs.utils.sprite import Sprite


def obscure(env: CatchEnv, pattern: str, padding=2, color=None, **kwargs):
    """
        Obscure the catch environment with a predefined pattern.

        :param env: (CatchEnv) environment to be obscured.
        :param pattern: (str) Named of the pattern to be added to the environment.
        :param padding: (int) Number of spaces between the patterns.
        :param color: (Tuple[int, ...]) color of the obscure obstacles.
        :param kwargs:
            - start: (int), start location of the pattern

        :return: (CatchEnv)
            Returns the obscured environment.
    """
    if pattern in ['raster', 'grid']:
        return obscure_raster(env, padding, color)
    if pattern == 'horizontal':
        return obscure_horizontal_lines(env, padding, color, **kwargs)
    if pattern == 'vertical':
        return obscure_horizontal_lines(env, padding, color, **kwargs)
    raise ValueError(f"Unknown obscure pattern {pattern!r}")


def obscure_raster(env: CatchEnv, padding=2, color=None):
    """ Adds a raster to the environment, with the same color as the goal.  """
    assert isinstance(env, CatchEnv), "Only works for CatchEnv environments."

    env = deepcopy(env)
    for x in range(1, env.grid.width - 1, padding):
        for y in range(1, env.grid.height - 1, padding):
            env.add_sprite(
                    name=f'Wall ({x:2d},{y:2d}',
                    sprite=Sprite(x=x, y=y, color=color or env.goal.color)
            )
    return env


def obscure_horizontal_lines(env: CatchEnv, padding=2, color=None, start=1):
    """ Adds equally spaced horizontal lines to the game, with the same color as the goal.   """
    assert isinstance(env, CatchEnv), "Only works for CatchEnv environments."

    env = deepcopy(env)
    for y in range(start, env.grid.height, padding):
        env.add_sprite(
                name=f'Wall ({0:2d}, {y:2d})',
                sprite=Sprite(x=0, y=y, width=env.grid.width, color=color or env.goal.color)
        )
    return env


def obscure_vertical_lines(env: CatchEnv, padding=None, color=None, start=0):
    """ Adds equally spaced vertical lines to the game, wih the same color as the goal.  """
    assert isinstance(env, CatchEnv), "Only works for CatchEnv."

    env = deepcopy(env)
    for x in range(start, env.grid.width, padding or env.grid.width):
        env.add_sprite(
                name=f'Wall ({x:2d}, {env.grid.height:2d})',
                sprite=Sprite(x=x, y=0, height=env.grid.height - 1, color=color or env.goal.color)
        )
    return env
