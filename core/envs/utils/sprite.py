from dataclasses import dataclass
from typing import Tuple


@dataclass
class Sprite:
    x: int = 0
    y: int = 0
    width: int = 1
    height: int = 1
    color: Tuple[int, ...] = None
    binary: bool = False

    @property
    def color_viewer(self):
        """ Return RGB values color, boolean become white and black, and gray values are repeated 3 times.  """
        if self.binary:
            return (self.color * 255,) * 3
        if isinstance(self.color, int):
            return (self.color,) * 3
        return self.color

    def relative(self, other, orientation='both'):
        """ Determine the row and column difference with respect to `self` object based on the orientation.  """
        if orientation.lower() == 'vertical':
            return self.y - other.y
        if orientation.lower() == 'horizontal':
            return self.x - other.x
        if orientation.lower() == 'both':
            return self.y - other.y, self.x - other.x
        raise ValueError(f"Unknown relative orientation {orientation!r}.")
