from collections import namedtuple

rgb = namedtuple('rgb', ('r', 'g', 'b'))


class Colors:
    """
        Helper class that holds a number of default colors.

        When you want to get other colors you can use `Colors(r, g, b)`
    """
    BLACK = rgb(0, 0, 0)
    WHITE = rgb(255, 255, 255)
    GRAY = rgb(127, 127, 127)
    RED = rgb(255, 0, 0)
    GREEN = rgb(0, 255, 0)
    BLUE = rgb(0, 0, 255)

    def __new__(self, r: int, g: int, b: int):
        return rgb(r, g, b)
