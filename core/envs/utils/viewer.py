from typing import List

import pyglet

from core.envs.utils import shapes
from core.envs.utils.fpstimer import FPSTimer
from core.envs.utils.sprite import Sprite


class SpriteViewer(pyglet.window.Window):
    fps = None

    def __init__(self, size_viewer=(400, 400), size_cell=(1, 1), sprites: List[Sprite] = None, **kwargs):
        super(SpriteViewer, self).__init__(*size_viewer, resizable=False, vsync=False, **kwargs)
        self._sprites = sprites
        self.width_viewer, self.height_viewer = size_viewer
        self.width_cell, self.height_cell = size_cell

        self.timer = FPSTimer(1e9)
        self.figures = []
        self.batch = pyglet.graphics.Batch()

        for sprite in self._sprites:
            x, y = sprite.x * self.width_cell, sprite.y * self.height_cell
            width, height = sprite.width * self.width_cell, sprite.height * self.height_cell
            shape = shapes.Rectangle(x, y, width, -height, sprite.color_viewer, batch=self.batch)
            self.figures.append((sprite, shape))

    def render(self):
        pyglet.clock.tick()
        self._update()
        self.switch_to()
        self.dispatch_events()
        self.dispatch_event('on_draw')
        self.flip()

        if self.fps is not None:
            # Very off for rates higher than 100, better to disable at that point.
            self.timer.update(self.fps)
            self.timer.sleep()

    def on_draw(self, *args, **kwargs):
        self.clear()
        self.batch.draw()

    def _update(self):
        for sprite, rect in self.figures:
            rect.position = sprite.x * self.width_cell, -sprite.y * self.height_cell + self.height_viewer
