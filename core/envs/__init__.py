import time
from pprint import pprint

import gym

from core.envs.catch import CatchEnv


def register_catch():
    """
        Register Catch environments to gym

        Available modes:

        >>> pprint(list((name for name in gym.envs.registry.env_specs if 'Catch' in name)))
        ['CatchDeterministic-20-20-v0',
         'CatchRandom-20-20-v0',
         'CatchDeterministic-20-20-v1',
         'CatchRandom-20-20-v1',
         'CatchDeterministic-20-20-v2',
         'CatchRandom-20-20-v2',
         'CatchDeterministic-20-20-v3',
         'CatchRandom-20-20-v3']
    """
    for version, display in enumerate(CatchEnv.metadata['display.modes']):
        for reset in CatchEnv.metadata['reset.modes']:
            for width, height in [(20, 20), (21, 21)]:
                gym.envs.register(
                        id=f"Catch{reset.capitalize()}-{width}-{height}-v{version}",
                        entry_point=f"core.envs:CatchEnv",
                        kwargs={
                            'grid': (height, width),
                            'display': display,
                            'reset': reset,
                        }
                )


# Can only register once to gym.
if not any('Catch' in name for name in gym.envs.registry.env_specs):
    register_catch()

if __name__ == '__main__':
    pprint(list((name for name in gym.envs.registry.env_specs if 'Catch' in name)))

    env = gym.make('CatchDeterministic-20-20-v3')
    env.reset()
    done = False
    while not done:
        obs, reward, done, info = env.step(env.action_space.sample())
        time.sleep(0.2)
        env.render()
