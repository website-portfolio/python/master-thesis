"""

Simple catch game based on:
    - https://www.groundai.com/project/an-initial-attempt-of-combining-visual-selective-attention-with-deep-reinforcement-learning/1

A toy problem called Catch was designed to study the properties of the CNN feature maps in deep reinforcement learning (see Figure 1).
A ball falls down from the top of the screen, and the agent controls the paddle at the bottom to catch the ball.
To be more specific:

    * State space: 20×20, with only black and white (0/1) pixels. The ball is of size 1×1 and the paddle is 3×1.
    * Initial state: at the beginning of each episode, the ball and the paddle are placed at a random position
        at the top and the bottom row respectively.
    * Action space: 3 actions, move the paddle 1 pixel left, stay, or move right.
    * Dynamics: the ball falls at the speed of 1 pixel per timestep.
    * Episode length: each episode ends when the ball reaches the bottom, thus it is fixed to 20 timesteps.
    * Reward: the player only receives reward at the end of episodes, +1 if the paddle catches the ball successfully,
        0 otherwise.

"""
from collections import namedtuple

import gym
import numpy as np

from .utils.constants import Colors
from .utils.sprite import Sprite
from .utils.viewer import SpriteViewer


class CatchEnv(gym.Env):
    size_ = namedtuple('size', ('width', 'height'))

    reward_range = (0, 1)
    metadata = {
        'render.modes': ['human', 'rgb_array', 'rgb_obs'],
        'display.modes': ['binary', 'gray', 'color', 'noisy'],  # Determine observation space output values.
        'reset.modes': ['deterministic', 'random']  # Determine if game is repeated with same starting position.
    }

    action_meanings = dict(NOOP=0, LEFT=1, RIGHT=2)
    colors = dict(
            binary=dict(goal=1, player=1, background=0),
            gray=dict(goal=255, player=255, background=0),
            color=dict(goal=Colors.BLUE, player=Colors.GREEN, background=Colors.BLACK),
            noisy=dict(goal=Colors.BLUE, player=Colors.GREEN, background=Colors.BLACK),
    )

    def __init__(self, grid=(20, 20), screen=(400, 400), display='color', reset='random', gaussian=(0, 5)):
        assert display in self.metadata['display.modes'], f"Mode {display!r} not in {self.metadata['display.modes']}"
        assert reset in self.metadata['reset.modes'], f"Reset mode {reset!r} not in {self.metadata['reset.modes']}"

        self.grid = self.size_(*grid)
        self.screen = self.size_(*screen)
        self.cell = self.size_(self.screen.width // self.grid.width, self.screen.height // self.grid.height)

        self.display_mode = display
        self.reset_mode = reset
        self.gaussian = gaussian
        self.viewer = None
        self._viewer_fps = None

        self.action_space = gym.spaces.Discrete(3)
        self.observation = self.observation_space.low.copy()
        self.observation_renderer = np.zeros((*self.screen, 3), dtype=np.uint8)

        self.spec = self._get_specs()

        color = self.colors[self.display_mode]
        boolean = self.display_mode == 'binary'
        self.goal = Sprite(x=0, y=0, width=1, height=1, color=color['goal'], binary=boolean)
        self.player = Sprite(x=0, y=self.grid.height - 1, width=3, height=1, color=color['player'], binary=boolean)
        self.background = Sprite(x=0, y=0, **self.grid._asdict(), color=color['background'], binary=boolean)

        # List of all game sprites, you can extend this one to add other items on the screen.
        # Note that the last items are drawn on top of the first items.
        self.sprites = dict(background=self.background, goal=self.goal, player=self.player, )

        # When deterministic these sprite values will never change again.
        self.goal_x, self.player_x = self.new_start_positions()

    def __str__(self):
        return f"{type(self).__name__}:" \
               f"\n\t- render: {self.metadata['render.modes']}" \
               f"\n\t- display mode: {self.metadata['display.modes']}" \
               f"\n\t- action space: {self.action_space}" \
               f"\n\t- shape: {self.observation_space}" \
               f"\n\t- reward range: {self.reward_range}"

    def __repr__(self):
        return f"<{type(self).__name__} (display={self.display_mode})>"

    def _get_specs(self):
        version = self.metadata['display.modes'].index(self.display_mode)
        env_id = f"Catch{self.reset_mode.capitalize()}-{self.grid.width}-{self.grid.height}-v{version}"
        try:
            return gym.spec(id=env_id)
        except gym.error.UnregisteredEnv:
            return env_id

    @property
    def observation_space(self):
        high = 1 if self.display_mode == 'binary' else 255
        shape = (*self.grid, 3) if self.display_mode in ['color', 'noisy'] else self.grid
        dtype = np.bool if self.display_mode == 'binary' else np.uint8
        return gym.spaces.Box(0, high, shape, dtype)

    @property
    def optimal_action(self):
        """ Returns the best action in the game at the current position.  """
        difference = self.goal.relative(self.player, orientation='horizontal')

        if difference == 1: return 0
        return 1 if difference < 1 else 2

    def step(self, action):
        assert self.goal.y <= self.grid.height - 1, f"Called step beyond the done signal."

        # Update sprite positions
        self.goal.y += 1
        self.player.x += 1 * int(action == 2) - 1 * int(action == 1)
        self.player.x = np.clip(self.player.x, a_min=0, a_max=self.grid.width - 3)

        # Determine the game state and rewards.
        self.observation = self._get_observation()
        info = dict(goal=self.goal, player=self.player)

        if self.goal.y >= self.grid.height:
            reward = int(0 <= self.goal.relative(self.player, orientation='horizontal') <= 2)
            return self.observation, reward, True, info
        return self.observation, 0, False, info

    def reset(self):
        if self.reset_mode == 'random':
            self.goal_x, self.player_x = self.new_start_positions()

        self.goal.x, self.goal.y = self.goal_x, 0
        self.player.x, self.player.y = self.player_x, self.player.y
        return self._get_observation()

    def render(self, mode='human', *args, **kwargs):
        if mode == 'rgb_array':
            return self._get_observation_renderer()

        if mode == 'rgb_obs':
            return self._get_observation()

        if self.viewer is None:
            self.viewer = SpriteViewer(self.screen, self.cell, self.sprites.values(), caption=f"Catch {self.grid}")
            self.viewer.fps = self._viewer_fps

        self.update_sprite_position()
        self.viewer.render()

    def update_sprite_position(self):
        """ Update the sprite position based on the sprites x and y value.  """
        for sprite in self.sprites.values():
            x = sprite.x * self.cell.width
            y = (-sprite.y - 1) * self.cell.height + self.screen.height
            sprite.position = x, y

    def new_start_positions(self):
        """ Returns random start positions (x coordinates) for the ball and paddle.  """
        random_goal_x = np.random.randint(0, self.grid.width)
        random_player_x = np.random.randint(0, self.grid.width - 3)
        return random_goal_x, random_player_x

    def close(self):
        if self.viewer is not None:
            self.viewer.close()
            self.viewer = None

    def set_fps(self, fps):
        self._viewer_fps = fps

    def add_sprite(self, name: str, sprite: Sprite):
        """
            Add additional sprites to the game (They have no dynamics, but are always rendered on top).
            The left top is (0, 0).
        """
        sprite.binary = self.display_mode == 'binary'
        self.sprites[name] = sprite

        # When a viewer is already active, you first have to close and reopen it to see any effect.
        if self.viewer is not None:
            self.viewer.close()
            self.viewer = None
            self.render()

    def add_noise(self, observation):
        observation += np.random.uniform(*self.gaussian, observation.shape).astype(np.uint8)
        return observation

    def _get_observation(self):
        self.observation[:, :] = self.background.color
        for sprite in self.sprites.values():
            (x_min, x_max), (y_min, y_max) = self._get_sprite_slice(sprite, size_cell=self.size_(1, 1))
            self.observation[y_min:y_max, x_min:x_max] = sprite.color

        if self.display_mode == 'noisy':
            return self.add_noise(self.observation)
        return self.observation.copy()

    def _get_observation_renderer(self):
        """ Return the renderer size observation instead.  """
        self.observation_renderer[:, :] = self.background.color_viewer
        for sprite in self.sprites.values():
            (x_min, x_max), (y_min, y_max) = self._get_sprite_slice(sprite, self.cell)
            self.observation_renderer[y_min:y_max, x_min:x_max] = sprite.color_viewer

        if self.display_mode == 'noisy':
            return self.add_noise(self.observation_renderer)
        return self.observation_renderer.copy()

    def _get_sprite_slice(self, sprite, size_cell):
        width = sprite.x * size_cell.width, (sprite.x + sprite.width) * size_cell.width
        height = sprite.y * size_cell.height, (sprite.y + sprite.height) * size_cell.height
        return width, height
