import logging
import os
import warnings

# Register Catch environments to gym.
from core.envs import CatchEnv

# Update tensorflow loggers level
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'  # '2' Error  | '3' Fatal
logging.getLogger('tensorflow').setLevel(logging.ERROR)
warnings.simplefilter(action='ignore', category=FutureWarning)

# Set core logger
logger = logging.getLogger('rl-base')
logger.setLevel(logging.INFO)

formatter = logging.Formatter("[%(levelname).1s] line %(levelno)4s, %(filename)-15s %(funcName)s: %(message)s")

ch = logging.StreamHandler()
ch.setFormatter(formatter)
logger.addHandler(ch)
