import os

import cv2


class VideoReader:
    def __init__(self, path):
        self.path = path

    def show(self, fps=30):
        """ Visualizes a stored video.  """
        cap = cv2.VideoCapture(self.path)
        basename = os.path.basename(self.path)

        while cap.isOpened():
            ret, frame = cap.read()
            if not ret: break

            cv2.imshow(basename, cv2.cvtColor(frame, cv2.COLOR_BGR2RGB))
            if cv2.waitKey(int(1_000 / fps)) & 0xFF == ord('q'):  break
            if cv2.getWindowProperty(basename, 0) < 0: break

        cap.release()
        cv2.destroyWindow(basename)

    def change_fps(self, output, fps=30):
        """ Creates a copy of the input video with the new frame rate.  """
        cap = cv2.VideoCapture(self.path)
        ret, frame = cap.read()

        out = self.create_source(output=output, size=frame.shape[:2], fps=fps)
        out.write(frame)

        while cap.isOpened():
            ret, frame = cap.read()
            if not ret: break
            out.write(frame)

        cap.release()
        out.release()
        cv2.destroyAllWindows()

    @staticmethod
    def create_source(output, size, fps=20., format='mp4v'):
        # Define the codec and create VideoWriter object
        fourcc = cv2.VideoWriter_fourcc(*format)
        if not os.path.exists(os.path.dirname(output)):
            os.makedirs(os.path.dirname(output))
        out = cv2.VideoWriter(output, fourcc, fps, size[::-1])
        return out
