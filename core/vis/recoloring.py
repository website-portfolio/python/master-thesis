import os
from typing import Dict, List, Union

import cv2
import numpy as np

from core import CatchEnv, utilities
from core.envs.utils.sprite import Sprite
from core.interface import Agent, Models
from core.vis.quiver import QuiverPlot
from core.vis.video import VideoReader
from core.wrappers import QuiverWrapper
from core.wrappers.quiver import QuiverWrapperAttention


class RecoloringQuiverPlot(QuiverPlot):
    def __init__(self, env: CatchEnv, agent: Union[Agent, Models], sprites, color_ranges):
        super().__init__(env, agent)
        self.agent = agent
        self._sprites: Dict[str, Sprite] = sprites
        self._color_ranges: List[Union[int, List[int]]] = color_ranges

    def get_wrapper(self, return_image, use_attention=False):
        if use_attention:
            return QuiverWrapperAttention(self.env, self.model, return_image=return_image)
        return QuiverWrapper(self.env, self.model, return_image=return_image)

    def get_recorder(self, filename, format, fps):
        width, height = self.env.screen[:2]
        filepath = filename or os.path.join('recording', self.env.spec.id, 'color wheel.mp4')
        out = VideoReader.create_source(output=filepath, size=(height, width * 2), fps=fps, format=format)
        return filepath, out

    def record(
            self,
            filename=None,
            fps=1,
            return_image='both',
            show=True,
            delay=1,
            format='mp4v',
            use_attention=False,
    ) -> str:
        """ record the changing color.  """
        filepath, out = self.get_recorder(filename, format, fps)

        env = self.get_wrapper(return_image, use_attention)
        env.reset()

        # Fix the goal position to prevent overlap with printed text.
        env.goal.x = self.env.grid.width - 2

        for sprites in utilities.change_colors.set_sprite_colors(self.env, self._sprites, self._color_ranges):
            frame = env.render(mode='quiver')

            for idx, (name, sprite) in enumerate(sprites.items()):
                text = f"{name}: {sprite.color}"
                cv2.putText(frame, text, (20, 40 + 40 * idx), cv2.FONT_HERSHEY_COMPLEX_SMALL, 1, (255, 0, 0), 1, 1)

            self.render(delay, frame, show)
            out.write(frame)

        out.release()
        cv2.destroyWindow('Recoloring quiver video')
        env.close()
        return filepath

    def play(
            self,
            filename=None,
            fps=1,
            return_image='both',
            show=True,
            delay=1,
            format='mp4v',
            use_attention=False
    ) -> str:
        """ Play a single game of the environment.   """
        filepath, out = self.get_recorder(filename, format, fps)
        env = self.get_wrapper(return_image, use_attention)
        env.reset()

        # Fix the goal position to prevent overlap with printed text.
        env.goal.x = self.env.grid.width - 2

        for steps in range(20):
            self.save_frame(delay, env, out, show)
            state = np.expand_dims(env.render('rgb_obs'), axis=0)
            env.step(self.agent.compute_action(state))

        out.release()
        cv2.destroyWindow('Recoloring quiver video')
        env.close()
        return filepath

    def save_frame(self, delay, env, out, show):
        frame = env.render(mode='quiver')
        out.write(frame)
        self.render(delay, frame, show)
        return frame

    def render(self, delay, frame, show):
        if show:
            cv2.imshow('Recoloring quiver video', cv2.cvtColor(frame, cv2.COLOR_RGB2BGR))
            cv2.waitKey(delay)
