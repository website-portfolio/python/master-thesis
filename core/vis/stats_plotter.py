from collections import defaultdict

import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
from IPython.core import display


class StatsPlotter:
    """
        Statistic plotter, that keeps track of a history object.

        For plotting stats it supports the following calls:

        - A single list or tuple with statistics to plot.
        - Single nested list, e.g. [['score', 'steps'], ..]]

    """

    def __init__(self, x_plot=None, plot_stats=(), plot_period=1, history=None):
        self.x_plot = x_plot
        self.plot_stats = plot_stats
        self.plot_period = plot_period
        self.history = history or defaultdict(list)
        self.stats = None

        # Create plot objects.
        self.fig, self.axs = self.setup_plots(self.plot_stats)

    def __len__(self):
        return len(self.plot_stats)

    def setup_plots(self, plot_stats):
        if plot_stats:
            fig, axs = plt.subplots(len(self), 1, squeeze=False, figsize=(10, 5 * len(self)))
            return fig, axs.ravel()
        return None, None

    def change_stats_to_plot(self, x_plot, plot_stats):
        """ Generate new statistic plotter.  """
        self.x_plot = x_plot
        if self.fig is not None: self.fig.close()
        self.fig, self.axs = self.setup_plots(plot_stats)

    def update_stats(self, stats: dict):
        """ Update the history stats with end of episode statistics.  """
        self.stats = stats

        for key, value in stats.items():
            self.history[key].append(value)

        # record totals per game
        total_episodes = self.history.get("total_episodes", [0])[-1] + 1
        self.history["total_episodes"].append(total_episodes)

        total_steps = self.history.get("total_steps", [0])[-1] + stats.get('steps_per_game', 0)
        self.history["total_steps"].append(total_steps)

    def show(self, episode=0, ipython=False, block=False):
        """ Display the physical plots or statistics, when using IPython, make sure to set ipython to True.  """
        if self.plot_stats is None: return self.history

        if self.plot_stats:
            return self._show_plot(block, episode, ipython)
        return self._show_text(episode)

    def close(self):
        plt.close(self.fig)

    def _show_plot(self, block, episode, ipython):
        if episode % self.plot_period != 0: return self.history

        for ax, stat_names in zip(self.axs, self.plot_stats):
            ax.clear()
            if isinstance(stat_names, (tuple, list)):
                self._plot_nested(stat_names, ax)
            else:
                self._plot_single(stat_names, ax)

        if ipython:
            display.display(self.fig)
            display.clear_output(wait=True)
        else:
            plt.show(block=block)
            plt.pause(1e-6)
        return self.history

    def _show_text(self, episode):
        if self.plot_stats:
            print(f"episode {episode:4d} | {self.stats}")
        return self.history

    def _plot_single(self, stat, ax):
        if self.x_plot is None:
            sns.lineplot(x=np.arange(len(self.history[stat])), y=self.history[stat], ax=ax)
        else:
            sns.lineplot(x=self.history[self.x_plot], y=self.history[stat], ax=ax)
        ax.set_title(stat)

    def _plot_nested(self, stats, ax):
        for stat in stats:
            self._plot_single(stat, ax)
        ax.set_title(stats)
        ax.legend(stats)
