from copy import deepcopy
from typing import Iterable, Callable, Union

import matplotlib.patches as mpatches
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from matplotlib.colors import Normalize

from core import utilities, CatchEnv
from core.envs.utils.sprite import Sprite
from core.interface import Models, Agent


class QuiverPlot:
    """ Helper to generate all possible ball predictions based on the custom environment Catch.  """

    def __init__(self, env: CatchEnv, model: Union[Agent, Models]):
        self.env = env
        self.model = model
        self.height, self.width = self.env.observation_space.shape[:2]

    @staticmethod
    def generate_quiver_data(data: pd.DataFrame, cmap='jet'):
        data['vec_x'] = data[['RIGHT']].sum(axis=1) - data[["LEFT"]].sum(axis=1)
        data['vec_y'] = -data[['NOOP']].sum(axis=1)
        data['vec_length'] = np.hypot(data['vec_x'], data['vec_y'])

        cmap: Callable = utilities.truncate_colormap(cmap=plt.get_cmap(cmap), min_val=0, max_val=0.5)
        func = lambda row: 'rgb(%d, %d, %d)' % tuple(map(lambda x: round(x * 255), cmap(row)[:3]))
        data['colors'] = data['vec_length'].apply(func)
        return data

    def verify_input(self, obs, sprite):
        assert sprite.width == sprite.height == 1, "Only works for 1x1 sprites."
        assert obs.size < 5_000, f"Has to generate {obs.size} image, which is probably too much (>16gb for 20x20 obs)"

    def all_possible_predictions(self, obs, sprite):
        """ Generate all possible states of the sprite.  Requires sprite to be 1 x 1.  """
        self.verify_input(obs, sprite)

        # Generate a copy of the environment without the sprite in it, and first axis free for copying.
        obs_copy = np.array(deepcopy(obs))
        state = np.expand_dims(obs_copy.astype(np.uint8), axis=0)
        state[:, sprite.y - 1, sprite.x] = self.env.background.color

        # Generate all possible sprite positions (numpy works with height, width)
        row, col = np.mgrid[:self.height, :self.width]
        z, row, col = np.arange(row.size, dtype=np.int32), row.flatten(), col.flatten()
        states = np.repeat(state, self.width * self.height, axis=0)
        states[z, row, col] = sprite.color

        data = self.generate_prediction_data(col, row, states)
        return data

    def all_possible_predictions_attention(self, obs, sprite: Sprite):
        """ Generate all possible sprites position with attention.  """
        self.verify_input(obs, sprite)

        # Store current location of sprites for restoring at the end.
        old_sprite_location = (sprite.x, sprite.y)

        # Generate all possible sprite positions (numpy works with height, width)
        states = []
        rows, columns = np.mgrid[:self.height, :self.width]
        for row, col in zip(rows.flatten(), columns.flatten()):
            sprite.x, sprite.y = col, row - 1
            state, *_ = self.env.step(0)
            states.append(state)

        # Restore the original position for continuing the game.
        sprite.x, sprite.y = old_sprite_location
        data = self.generate_prediction_data(columns.flatten(), rows.flatten(), np.array(states))
        return data

    def generate_prediction_data(self, columns, rows, states):
        # Required to determine the best action that is predicted by the model.
        action_meaning = self.env.action_meanings
        meaning_action = dict(map(reversed, action_meaning.items()))
        predictions = self.model.get_action_probabilities(states)
        data = pd.DataFrame(predictions, columns=action_meaning)
        data['action'] = [meaning_action[idx] for idx in np.argmax(predictions, axis=1)]
        data['y'], data['x'] = np.abs(rows - 1 - rows.max()), columns
        return data

    def generate_sprite_patch(self, sprite: Sprite, **kwargs):
        """ Create a patch to show the player on the matplotlib image. """
        kwargs = {**dict(linewidth=1, color='k', fill=False), **kwargs}
        return mpatches.Rectangle((sprite.x - 0.5, self.height - sprite.y - 0.5), sprite.width, sprite.height, **kwargs)

    def plot(self, quiver, limits, patches: Iterable['mpatches'] = (), cmap='jet', show=True):
        """
            Display a quiver plot.

            :param quiver: x, y, vex_x, vec_y, vec_length
                Data for plotting the quiver information.
            :param patches: plt.patch
                Patches that have to be added to the image.
            :returns: np.ndarray
                The output image as a numpy array.
        """
        fig, ax = plt.subplots()
        ax.set_title(" ")
        ax.scatter(quiver['x'], quiver['y'], color='1', s=1)

        # All quivers are now pointing in the action direction they want to move (NOOP is no direction).
        quiver['vec_x'] = (quiver['action'].isin(['RIGHT', 'LEFT'])).astype(int)
        quiver['vec_x'] *= ((quiver['action'] == 'RIGHT') * 2 - 1).astype(int)
        quiver['vec_y'] = (quiver['action'].isin(['UP', 'DOWN'])).astype(int)
        quiver['vec_y'] *= ((quiver['action'] == 'UP') * 2 - 1).astype(int)

        # Quiver arrows and color bar
        cmap = utilities.truncate_colormap(cmap=plt.get_cmap(cmap), min_val=0, max_val=0.5)
        im = ax.quiver(quiver['x'], quiver['y'], quiver['vec_x'], quiver['vec_y'], quiver['vec_length'],
                       norm=Normalize(vmin=0, vmax=1), cmap=cmap, width=0.005, scale=1.1, scale_units='x', pivot='mid')

        ax.set_xlim(limits[0])
        ax.set_ylim(limits[1])

        color_bar = fig.colorbar(im, ax=ax)
        color_bar.set_label('Confidence')

        # Add patches:
        for patch in patches:
            ax.add_patch(patch)

        if show: fig.show()
        return fig
