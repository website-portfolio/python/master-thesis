from typing import Optional

import numpy as np
import pandas as pd
import plotly.graph_objs as go


class ColorsDistribution:
    """ Display the color distribution of the input data.  """

    def __init__(self, data: Optional[np.ndarray] = None):
        self.data = data

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        return False

    @property
    def data(self):
        return self._data

    @data.setter
    def data(self, arr: np.ndarray):
        if not isinstance(arr, np.ndarray):
            arr = np.array(arr)
        if len(arr.shape) < 3 or arr.shape[-1] not in range(1, 4):
            arr = np.expand_dims(arr, axis=-1)
        if arr.min(initial=None) < 0 or arr.max(initial=None) > 255:
            raise ValueError(f"The array values have to be in the range 0 and 255.")
        self._data = arr

    @staticmethod
    def func_rgb(row: pd.Series):
        """ Returns a xyz row converted to a plotly rgb value.  """
        return f"rgb({row.x},{row.y},{row.z})"

    @staticmethod
    def func_hex(row: pd.Series):
        """ Returns a xyz row converted to a hex value.  """
        return f"hex({('#%02x%02x%02x' % (row.x, row.y, row.z)).upper()})"

    def figure(self, labels=('Red', 'Green', 'Blue'), show=True, return_data=False, return_trace=False):
        """
            Generates the ColorDistribution figure based on the data.

        :param labels:  Tuple[str, ...]
            The values that have to be displayed on the axis.
        :param show: bool
            If True it will show the plot in the browser.
        :param return_data: bool
            If True it returns the dataframe of the color distribution.

        :return:
            A figure object and if `return_data` is True, it will return the
            dataframe.

            The figure will depend on the number of dimensions in the last axis:

                1D: A Bar plot with on the x axis the range and y value the counts.

                2D: A 3D histogram plot where the z axis depict the counts.

                3D: A 3D scatter plot with the x, y and z axis representing all possible color combinations
                    and the size of the marker represents the count (if using Plotly), otherwise there
                    is no count indication.

        """
        df = self.generate_data()
        if self._data.shape[-1] == 1:
            fig = self.plot_1d(df, labels=labels, show=show, return_trace=return_trace)
        elif self._data.shape[-1] == 2:
            fig = self.plot_2d(df, labels=labels, show=show, return_trace=return_trace)
        elif self._data.shape[-1] == 3:
            df['rgb'] = df.apply(self.func_rgb, axis=1, result_type='expand')
            df['hex'] = df.apply(self.func_hex, axis=1, result_type='expand')
            fig = self.plot_3d(df, labels, show=show, return_trace=return_trace)
        else:
            raise ValueError(f"Data shape is not recognized.")

        if return_data:
            return fig, df
        return fig

    def generate_data(self):
        data_long = self._data.reshape(-1, self._data.shape[-1])
        data_unique, counts = np.unique(data_long, axis=0, return_counts=True)
        data_xyz = np.rollaxis(data_unique, axis=1)

        df_data = {key: val.flatten() for key, val in zip('xyz', data_xyz)}
        df_data['counts'] = counts
        df = pd.DataFrame(data=df_data)
        return df

    @staticmethod
    def plot_1d(df, labels, show=True, return_trace=False):
        """ Returns a bar plot, that shows the unique values.  """
        trace = go.Bar(x=df.x, y=df.counts)
        if return_trace: return trace

        fig = go.Figure(trace)
        fig.update_layout(scene=dict(**{f"{ax}axis": dict(title=label) for ax, label in zip(['x'], labels)}))
        if show: fig.show()

        return fig

    @staticmethod
    def plot_2d(df, labels, show=True, return_trace=False):
        """ Returns a 3D plot of image color distribution, where x and y are the values and z the count. """
        trace = go.Scatter3d(
                x=df.x, y=df.y, z=df.counts,
                marker=dict(size=np.clip(df.counts, a_min=5, a_max=50)),
                mode='markers'
        )
        if return_trace: return trace

        fig = go.Figure(data=trace)
        fig.update_layout(scene=dict(**{f"{ax}axis": dict(title=label) for ax, label in zip('xy', labels)}))
        if show: fig.show()

        return fig

    @staticmethod
    def plot_3d(df, labels=('Red', 'Green', 'Blue'), show=True, return_trace=False):
        """ Returns a 3D plot of image color distribution in RGB values, with size as count indicator.  """
        trace = go.Scatter3d(
                x=df.x, y=df.y, z=df.z,
                hovertemplate="%{customdata[0]}<br>"
                              "%{customdata[1]}<br>"
                              "Counts: %{customdata[2]:3d}<extra></extra>",
                customdata=df[['rgb', 'hex', 'counts']],
                marker=dict(color=df['rgb'], opacity=0.6,
                            size=np.clip(np.log2(df.counts), a_min=5, a_max=50)),
                mode='markers'
        )
        if return_trace: return trace

        fig = go.Figure(data=trace, layout=dict(showlegend=False))
        fig.update_layout(scene=dict(**{f"{ax}axis": dict(title=label) for ax, label in zip('xyz', labels)}))
        if show: fig.show()

        return fig
