import itertools
import pprint
import unittest

import tqdm

from core.learn.random.agents import RandomAgent
from core.run import run_episode
from experiments.notebooks import utils


class EnvCreate(unittest.TestCase):
    metadata = {
        'display': ['binary', 'gray', 'color'],
        'attention': [None, 'position', 'color', 'double'],
        'reset': ['deterministic', 'random'],
        'obscure_pattern': [None, 'raster', 'horizontal', 'vertical'],
        'obscure_color': [None],
        'frame_stack': [None, 1, 4],
        'training': [True, False],
        'inverse': [True, False],
    }

    def test_options(self):
        settings = [dict(zip(self.metadata.keys(), values)) for values in itertools.product(*self.metadata.values())]
        for setting in tqdm.tqdm(settings):

            # Attention is not meant for binary display.
            if setting['display'] == 'binary' and setting['attention'] is not None:
                continue

            with self.subTest(pprint.pformat(setting)):
                env = utils.create_env(**setting)
                agent = RandomAgent(env=env)
                run_episode(env, agent)


if __name__ == '__main__':
    unittest.main()
