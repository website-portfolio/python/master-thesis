import os
import unittest

import matplotlib.pyplot as plt
import numpy as np

from core import utilities
from core.attention.gaussian import Gaussian


class TestGaussian(unittest.TestCase):

    @classmethod
    def setUpClass(cls) -> None:
        cls.folder_test_data = os.path.join(os.path.dirname(__file__), 'test_data')

    def tearDown(self) -> None:
        plt.close()

    def test_1d_single(self):
        gaussian = Gaussian(size=(20,))
        dist = gaussian.create(dim=1)
        fig = gaussian.plot1d(dist, show=False)
        arr = utilities.fig_to_numpy(fig, dpi=180)

        result_arr = np.load(os.path.join(self.folder_test_data, 'test_1d.npy'))
        self.assertEqual(True, np.array_equal(arr, result_arr), f"Value mismatch in array")

    def test_1d_single_off_center(self):
        gaussian = Gaussian(size=(20,))
        dist = gaussian.create(dim=1, center=(1,))
        fig = gaussian.plot1d(dist, show=False)

    def test_1d_multiple(self):
        gaussian = Gaussian(size=(20,))
        with self.assertRaises(ValueError):
            gaussian.creates(dim=1, centers=(1, 19))

    def test_2d_single(self):
        gaussian = Gaussian(size=(20, 20))
        dist = gaussian.create(dim=2)
        fig = gaussian.plot2d(dist, show=False)
        arr = utilities.fig_to_numpy(fig, dpi=180)

        result_arr = np.load(os.path.join(self.folder_test_data, 'test_2d.npy'))
        self.assertEqual(True, np.array_equal(arr, result_arr), f"Value mismatch in array")

    def test_2d_single_off_center(self):
        gaussian = Gaussian(size=(20, 20))
        dist = gaussian.create(dim=2, center=((1, 3)))
        fig = gaussian.plot2d(dist, show=False)

    def test_2d_multiple(self):
        gaussian = Gaussian(size=(20, 20))
        dist = gaussian.creates(dim=2, centers=((1, 1), (19, 19)))
        fig = gaussian.plot(dist, show=False)

    def test_3d_single(self):
        gaussian = Gaussian(size=(20, 20, 20))
        dist = gaussian.create(dim=3)
        fig = gaussian.plot3d(dist, show=False)
        arr = utilities.fig_to_numpy(fig, dpi=180)

        result_arr = np.load(os.path.join(self.folder_test_data, 'test_3d.npy'))
        self.assertEqual(True, np.array_equal(arr, result_arr), f"Value mismatch in array")

    def test_3d_single_off_center(self):
        gaussian = Gaussian(size=(20, 20, 20))
        dist = gaussian.create(dim=3, center=((1, 3, 7)))
        fig = gaussian.plot3d(dist, show=False)

    def test_3d_single_small(self):
        gaussian = Gaussian(size=(20, 20, 5))
        dist = gaussian.create(dim=3)
        fig = gaussian.plot3d(dist, show=False)
        arr = utilities.fig_to_numpy(fig, dpi=180)

        result_arr = np.load(os.path.join(self.folder_test_data, 'test_3d_small.npy'))
        self.assertEqual(True, np.array_equal(arr, result_arr), f"Value mismatch in array")

    def test_3d_multiple_small(self):
        gaussian = Gaussian(size=(20, 10, 5))
        dist = gaussian.creates(dim=3, centers=((1, 1, 2), (19, 9, 2), (10, 5, 2.5)))
        fig = gaussian.plot(dist, show=False)
        arr = utilities.fig_to_numpy(fig, dpi=180)

        result_arr = np.load(os.path.join(self.folder_test_data, 'test_3d_multiple_small.npy'))
        self.assertEqual(True, np.array_equal(arr, result_arr), f"Value mismatch in array")

    def test_plot_dimensions_inference(self):
        for dim in range(1, 4):
            shape = np.ones(dim) * 20
            gaussian = Gaussian(size=shape)
            dist = gaussian.create(dim=dim)
            fig = gaussian.plot(dist, show=False)

            arr = utilities.fig_to_numpy(fig, dpi=180)
            self.assertEqual((864, 1152, 3), arr.shape, f"Array shape is not consistent, dim {dim}, shape {shape}")

            result_arr = np.load(os.path.join(self.folder_test_data, f"test_{dim}d.npy"))
            self.assertEqual(True, np.array_equal(arr, result_arr), f"Value mismatch in array")


if __name__ == '__main__':
    unittest.main()
