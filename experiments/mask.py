from experiments.utils.running import run_dqn_experiment

if __name__ == '__main__':
    color_ranges_rgb = [
        ((0, 0, 0), (255, 255, 255), (8, 8, 8)),  # Background
        ((0, 0, 0), (255, 255, 255), (8, 8, 8)),  # Ball (goal)
        ((0, 0, 0), (255, 255, 255), (8, 8, 8)),  # Paddle (player)
    ]

    color_ranges_gray = [
        (0, 255, 256), (0, 255, 256), (0, 255, 256)
    ]

    for network in ['dqn']:
        for display in ['gray', 'color']:
            for attention in ['position', 'color', 'double']:
                print(f"\n\nAblation {network!r} with {attention!r} attention, for {display!r} environment.")

                color_ranges = color_ranges_gray if display == 'gray' else color_ranges_rgb
                kwargs = dict(kwargs_env=dict(display=display, attention=attention, attention_mask=True),
                              color_ranges=color_ranges, network=network)
                run_dqn_experiment(**kwargs, mode='ablation')

    # Only allow combinations for limited gray region, since color values take a lot of time
    # (see `utils.calculate_colors` for estimation time).

    color_ranges = [
        (0, 255, 16), (0, 255, 16), (0, 255, 16)
    ]

    for network in ['dqn']:
        for display in ['gray']:
            for attention in ['position', 'color', 'double']:
                print(f"\n\nCombinations {network!r} with {attention!r} attention, for {display!r} environment.")

                kwargs = dict(kwargs_env=dict(display=display, attention=attention, attention_mask=True),
                              color_ranges=color_ranges, network=network)
                run_dqn_experiment(**kwargs, mode='combined')
