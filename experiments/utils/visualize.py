import logging
from typing import Dict

import matplotlib.pyplot as plt
import pandas as pd
import plotly.graph_objs as go
from plotly.subplots import make_subplots

from core.environment import create_env
from core.run import run_experiment
from core.utilities import describe_history
from core.wrappers.attention import *
from experiments.utils.storing import load_from_folder


def rerun_agent(folder, runs=100, train=False, **kwargs):
    """ Run evaluation of a trained agent.  """
    data = load_from_folder(folder, agent=True, settings=True)
    env, _ = create_env(**data.settings['env'])
    history = run_experiment(env, agent=data.agent, runs=runs, train=train, **kwargs)
    results = describe_history(history, show=False)
    return results


def plot_data(
        result: pd.DataFrame,
        metric='mean',
        merge=False,
        show=True,
        min_score=None,
):
    """ Shows subplots of all the sprites and the score per color.  """
    summary = result.copy() if min_score is None else result[result.score >= min_score].copy()
    summary = summary.loc[summary.index == metric]
    sprites: List[str] = ('background', 'goal', 'player')

    if not summary.size:
        logging.warning(f"No results that match your criteria:\n\t- Metric: {metric}\n\t- Min score: {min_score}")
        return None
    if summary.values.size and summary[next(iter(sprites))].dtype == np.int64:
        if merge: return plot_gray_merged(summary, sprites, show)
        return plot_gray(summary, sprites, show)
    if summary.values.size and summary[next(iter(sprites))].dtype == np.object:
        if merge: raise ValueError(f"Color mode has no merged variant.")
        return plot_color(result.loc[result.index == metric], sprites, min_score, show)
    raise ValueError(f"Unknown color type")


# Helper functions

def get_chunked_summary(summary, sprites=None, chunks=3):
    for pos, sprite in zip(range(0, len(summary), len(summary) // chunks), ('background', 'goal', 'player')):
        if sprites is None or sprite in sprites:
            yield summary[pos:pos + len(summary) // chunks].copy()


def get_color_scale(min_val=0, max_val=0.5, cmap='jet'):
    """ Truncate a colormap.  """
    cmap = utilities.truncate_colormap(cmap=plt.get_cmap(cmap), min_val=min_val, max_val=max_val)
    color_scale = [[i / 256, f"rgb{tuple(map(lambda x: int(x * 256), cmap(i)[:3]))}"] for i in range(256 + 1)]
    return color_scale


def get_scene_layout(labels):
    axis_settings = dict()
    for ax, label in zip('xyz', labels):
        axis_settings[f"{ax}axis"] = dict(title=label, range=(0, 260), dtick=50, autorange=False)
    return dict(aspectratio=dict(x=1, y=1, z=1), **axis_settings)


def get_marker_layout(summary, color_scale, x=1.05):
    return dict(
            color=summary.score, opacity=0.6, colorscale=color_scale,
            size=summary.score.apply(lambda score: max(4, score * 15)),
            colorbar=dict(tick0=0, dtick=0.2, x=x, title='Reward'),
            autocolorscale=False, cmin=0, cmax=1
    )


def get_trace_gray_2d(summary, color_scale, sprite):
    return go.Scatter(
            x=summary[sprite], y=summary.score,
            hovertemplate="Gray value: %{customdata[0]:3d}<br>"
                          "Reward: %{customdata[1]}<extra></extra>",
            customdata=summary[[sprite, 'score']],
            hoverlabel=dict(bgcolor=summary['rgb']),
            marker=get_marker_layout(summary, color_scale),
            line=dict(color='blue', width=0.2),
            mode='lines+markers',
    )


def get_trace_gray_3d(summary, color_scale, sprites, x=1.05):
    return go.Scatter3d(
            x=summary['background'], y=summary['goal'], z=summary['player'],
            hovertemplate="Background: %{customdata[0]:3d}<br>"
                          "Ball: %{customdata[1]:3d}<br>"
                          "Paddle: %{customdata[2]:3d}<br>"
                          "Reward: %{customdata[3]:.3f}<extra></extra>",
            customdata=summary[[*sprites, 'score']],
            marker=get_marker_layout(summary, color_scale, x=x),
            mode='markers',
    )


def get_trace_color(summary, color_scale):
    return go.Scatter3d(
            x=summary.r, y=summary.g, z=summary.b,
            hovertemplate="Color: %{customdata[0]}<br>"
                          "Reward: %{customdata[1]:.3f}<extra></extra>",
            customdata=summary[['rgb', 'score']],
            hoverlabel=dict(bgcolor=summary['rgb']),
            marker=get_marker_layout(summary, color_scale),
            mode='markers',
    )


def plot_gray(summary, sprites, show):
    specs = [[dict(type='xy')] for _ in range(3)]
    color_scale = get_color_scale()
    windows = get_chunked_summary(summary)

    fig = make_subplots(rows=3, cols=1, subplot_titles=sprites, specs=specs)
    fig.update_layout(showlegend=False)
    fig.update_xaxes(title=dict(text='Gray value'), range=(-10, 260), dtick=50, autorange=False)
    fig.update_yaxes(title=dict(text='Reward'), range=(0, 1.1), dtick=.2, autorange=False)

    for row, (sprite, window) in enumerate(zip(sprites, windows), start=1):
        window['rgb'] = window.apply(lambda row: f"rgb{str((row[sprite],) * 3)}", axis=1, result_type='expand')
        fig.add_trace(get_trace_gray_2d(window, color_scale, sprite), row=row, col=1)

    if show: fig.show()
    return fig


def plot_gray_merged(summary, sprites, show):
    color_scale = get_color_scale()
    trace = get_trace_gray_3d(summary, color_scale, sprites, x=0.8)

    fig = go.Figure(data=trace, layout=dict(showlegend=False, title=None))
    fig.update_layout(scene=get_scene_layout(sprites))

    if show: fig.show()
    return fig


def plot_color(summary, sprites, min_score=None, show=True):
    color_scale = get_color_scale()
    scene_layout = get_scene_layout(labels='rgb')
    windows = get_chunked_summary(summary)

    fig = make_subplots(rows=1, cols=3, subplot_titles=sprites, specs=[[dict(type='scene') for _ in range(3)]])
    fig.update_layout(showlegend=False, scene=scene_layout, scene2=scene_layout, scene3=scene_layout)

    for column, (sprite, window) in enumerate(zip(sprites, windows), start=1):
        result = window.copy() if min_score is None else window[window.score >= min_score].copy()
        color = result[sprite].str[1:-1].str.split(',')
        result[['r', 'g', 'b']] = pd.DataFrame(dict(r=color.str[0], g=color.str[1], b=color.str[2])).astype(int)
        result['rgb'] = result.apply(lambda row: f"rgb({row.r},{row.g},{row.b})", axis=1, result_type='expand')
        fig.add_trace(get_trace_color(result, color_scale), row=1, col=column)

    if show: fig.show()
    return fig


def plot_merge_data(
        results: Dict[str, pd.DataFrame],
        sprites=('background', 'goal', 'player'),
        metric='mean',
        show=True
):
    """ Merging gray ablation figures.  """
    rows, cols = len(sprites), len(results)
    results = {key or 'baseline': value for key, value in results.items()}
    specs = [[dict(type='xy') for _ in range(len(results))] for _ in range(len(sprites))]
    color_scale = get_color_scale()

    fig = make_subplots(rows=rows, cols=cols, specs=specs, column_titles=tuple(results), row_titles=sprites)
    fig.update_layout(showlegend=False)
    fig.update_xaxes(title=dict(text='Gray value'), range=(-10, 260), dtick=50, autorange=False)
    fig.update_yaxes(title=dict(text='Reward'), range=(0, 1.1), dtick=.2, autorange=False)

    for column, (key, summary) in enumerate(results.items(), start=1):
        assert summary[next(iter(sprites))].dtype.kind == 'i', "Only available for gray colors."
        summary = summary.loc[summary.index == metric]
        windows = get_chunked_summary(summary, sprites)

        for row, (sprite, window) in enumerate(zip(sprites, windows), start=1):
            window['rgb'] = window.apply(lambda row: f"rgb{str((int(row[sprite]),) * 3)}", axis=1, result_type='expand')
            trace = get_trace_gray_2d_merge(window, color_scale, sprite, name=key, showlegend=not row - 1)
            fig.add_trace(trace, row=row, col=column)

    if show: fig.show()
    return fig


def get_trace_gray_2d_merge(summary, color_scale, sprite, name, showlegend=True):
    # colors = dict(dqn='black', double_dqn='green', dueling_dqn='red', d3qn='blue')
    return go.Scatter(
            x=summary[sprite], y=summary.score,
            hovertemplate="Gray value: %{customdata[0]:3d}<br>"
                          "Reward: %{customdata[1]}<extra></extra>",
            customdata=summary[[sprite, 'score']],
            hoverlabel=dict(bgcolor=summary['rgb']),
            marker=get_marker_layout(summary, color_scale, x=1.03),
            legendgroup=name or 'baseline', showlegend=showlegend,
            line=dict(color='black', width=0.2),
            mode='lines+markers',
            name=name if name is not None else 'baseline',
    )
