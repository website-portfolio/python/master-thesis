from typing import Dict

import pandas as pd
import tqdm

from core import utilities, CatchEnv
from core.environment import create_env
from core.envs.utils.sprite import Sprite
from core.interface import Agent
from core.run import run_experiment, run_q_learning
from experiments.utils.calculate_colors import calculate_counts
from experiments.utils.storing import get_save_folder, save_data


def _get_colors_and_counts(env, sprites, color_ranges, mode):
    counts = calculate_counts(env.display_mode, color_ranges, mode)

    if mode == 'ablation' or mode == 0:
        colors = utilities.change_colors.set_sprite_colors(env, sprites, color_ranges)
        return colors, counts

    if mode == 'combined' or mode == 1:
        colors = utilities.change_colors.set_multi_sprite_colors(env, sprites, color_ranges)
        return colors, counts

    raise ValueError(f"Unknown mode: {mode!r}")


def run_recolor_evaluation(
        env: CatchEnv,
        agent: Agent,
        sprites: Dict[str, Sprite],
        color_ranges,
        mode='ablation',
        train=False,
        **kwargs
):
    """ Evaluate an agent on multiple color ranges.  """
    temp_results = []
    colors, counts = _get_colors_and_counts(env, sprites, color_ranges, mode)

    for sprites in tqdm.tqdm(colors, total=counts, desc="Recoloring"):
        history = run_experiment(env, agent, train=train, **kwargs)
        summary = utilities.describe_history(history, show=False)

        for name, sprite in sprites.items():
            summary[name] = [tuple(sprite.color)] * len(summary) if isinstance(sprite.color, tuple) else sprite.color
        temp_results.append(summary)

    result = pd.concat(temp_results)
    return result


def run_dqn_experiment(kwargs_env, network, color_ranges, mode, runs=1_000):
    """ Basic run methods for different environmental settings, dqn network and color ranges.  """
    env, env_settings = create_env(frame_stack=1, training=True, **kwargs_env)
    agent, history, q_settings = run_q_learning(env, runs=runs, network=network, use_epsilon=True)

    agent.use_epsilon = False
    sprites = dict(background=env.background, goal=env.goal, player=env.player)
    results = run_recolor_evaluation(env, agent, sprites, color_ranges, train=False, plot_stats=False, mode=mode)

    # Saving data.
    save_folder = get_save_folder(env, agent, create=True)
    save_data(agent=agent, results=results, save_folder=save_folder, settings=dict(
            dict(agent=agent.__class__.__name__, env_name=env.spec.id, env=env_settings, dqn=q_settings, mode=mode)
    ))
