"""
    Helper script to calculate the number of color combinations
    that have to be evaluated.
"""

import numpy as np

from core import CatchEnv, utilities


def calculate_ablation(env, sprites, color_ranges, show=False):
    if not show: return calculate_counts(env.display_mode, color_ranges, mode='ablation')

    counts = 0
    for sprites in utilities.change_colors.set_sprite_colors(env, sprites, color_ranges):
        if show:  print(list((name, sprite.color) for name, sprite in sprites.items()))
        counts += 1

    return counts


def calculate_combined(env, sprites, color_ranges, show=False):
    if not show: return calculate_counts(env.display_mode, color_ranges, mode='combined')

    counts = 0
    for sprites in utilities.change_colors.set_multi_sprite_colors(env, sprites, color_ranges):
        if show: print(list((name, sprite.color) for name, sprite in sprites.items()))
        counts += 1

    return counts


def calculate_counts(display, color_ranges, mode):
    if mode == 'ablation':
        if display == 'gray':
            return sum([num for start, end, num in color_ranges])
        return np.sum([np.product(color_range[-1]) for color_range in color_ranges])

    if mode == 'combined':
        if display == 'gray':
            return np.product([num for start, end, num in color_ranges])
        return np.product([np.product(color_range[-1]) for color_range in color_ranges])

    raise ValueError(f"Unknown mode {mode!r} or display {display!r}")


def calculate_time(counts):
    # 1500 items take around 30 min
    per_hour = 3000
    hours, min = divmod(counts, per_hour)
    print(f"Estimated time for completion: ~{hours:2d} hours and {int(min / per_hour * 60):02d} min", end='\n\n')


# The last values should all be lower than
# 4 to stay within 3 days for combined
# 28 to stay within 1 day for ablation.
color_ranges_rgb = [
    ((0, 0, 0), (255, 255, 255), (8, 8, 8)),  # Background
    ((0, 0, 0), (255, 255, 255), (8, 8, 8)),  # Ball (goal)
    ((0, 0, 0), (255, 255, 255), (8, 8, 8)),  # Paddle (player)
]

color_ranges_gray = [
    (0, 255, 16), (0, 255, 16), (0, 255, 16)
]

if __name__ == '__main__':
    display = 'gray'

    env = CatchEnv(display=display)
    sprites = dict(background=env.background, goal=env.goal, player=env.player)
    color_ranges = color_ranges_gray if display == 'gray' else color_ranges_rgb

    counts = calculate_ablation(env, sprites, color_ranges, show=True)
    print(f"\nAblation, number of elements: {counts:8,d}")
    calculate_time(counts)

    counts = calculate_combined(env, sprites, color_ranges, show=False)
    print(f"\nCombined, number of elements: {counts:8,d}")
    calculate_time(counts)
