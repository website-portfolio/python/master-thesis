import glob
import json
import os
from argparse import Namespace
from datetime import datetime
from typing import Union

import gym
import pandas as pd

from core import CatchEnv
from core.environment import create_env
from core.interface import Agent
from core.learn.imitation.agents import ImitationAgent
from core.learn.imitation.models import NeuralNetwork
from core.run import run_q_learning


def get_save_folder(env: Union[CatchEnv, gym.Env], agent: Agent, create=False, exists_ok=False):
    path = os.path.join(
            'data',
            agent.__class__.__name__,
            env.unwrapped.spec.id,
            datetime.now().strftime('%Y-%m-%d %H.%M.%S')
    )

    if create:
        if not exists_ok and os.path.exists(path):
            raise FileExistsError(f"Path already exists {path!r}")
        os.makedirs(path)

    return path


def save_data(agent=None, results: pd.DataFrame = None, settings=None, save_folder='data', exists_ok=True):
    if save_folder is not None:
        os.makedirs(save_folder, exist_ok=exists_ok)

    if results is not None:
        results.to_csv(os.path.join(save_folder, 'results.csv'), sep=';', index=True)

    if settings is not None:
        with open(os.path.join(save_folder, 'metadata.json'), 'w') as file:
            json.dump(settings, file)

    if agent is not None:
        agent.save_model(os.path.join(save_folder, 'model'))

    return save_folder


def load_agent(save_folder, settings):
    env, _ = create_env(**settings['env'])
    if 'dqn' in settings:
        settings['dqn'].update(dict(runs=0, use_epsilon=False))
        agent, history, settings = run_q_learning(env, **settings['dqn'])
        agent.load_model(os.path.join(save_folder, 'model'))
        return agent

    if 'imitation' in settings:
        model = NeuralNetwork(**settings['model'])
        settings['dqn'].update(dict(trained=True))
        return ImitationAgent(env, model, **settings['imitation'])

    return None


def load_settings(save_folder):
    path = os.path.join(save_folder, 'metadata.json')
    if not os.path.exists(path):
        return None

    with open(path, 'r') as file:
        settings = json.load(file)
    return settings


def load_results(save_folder):
    path = os.path.join(save_folder, 'results.csv')
    if os.path.exists(path):
        return pd.read_csv(path, sep=' *;', index_col=0, skipinitialspace=True, engine='python')
    return None


def load_from_folder(save_folder, settings=False, agent=False, results=False):
    settings_ = load_settings(save_folder=save_folder)

    return Namespace(
            agent=load_agent(save_folder, settings_) if agent else None,
            results=load_results(save_folder) if results else None,
            settings=settings_ if settings else None,
    )


def load_data_from_settings(match, folder='*', env_name='*', return_settings=False):
    """ Compares all known files against provided settings.  """
    folders, settings = [], []
    for folder in glob.iglob(f"data/{folder}/{env_name}/*"):
        loaded_settings = load_settings(save_folder=folder)

        if compare_dict(loaded_settings, match):
            folders.append(folder)
            settings.append(loaded_settings)

    if return_settings:
        return folders, settings
    return folders


def compare_dict(original, match):
    for key, value in match.items():
        if key not in (original or []):  return False
        if value == True: continue
        if value == original[key]: continue

        if isinstance(value, dict):
            if not compare_dict(original[key], value):
                return False

        # One of condition
        elif isinstance(value, list):
            if not any(val == original[key] for val in value):
                return False

        # all condition
        elif isinstance(value, tuple):
            if sorted(value) != sorted(original[key]):
                return False

        elif isinstance(value, (str, int, float, type(None))):
            if value != original[key]:
                return False

        else:
            raise TypeError(f"Unknown type: {type(value)!r}, for value: {value}")

    return True
