from experiments.utils.running import run_dqn_experiment

if __name__ == '__main__':
    color_ranges_rgb = [
        ((0, 0, 0), (255, 255, 255), (8, 8, 8)),  # Background
        ((0, 0, 0), (255, 255, 255), (8, 8, 8)),  # Ball (goal)
        ((0, 0, 0), (255, 255, 255), (8, 8, 8)),  # Paddle (player)
    ]

    color_ranges_gray = [
        (0, 255, 256), (0, 255, 256), (0, 255, 256)
    ]

    for network in ['dqn', 'dueling_dqn', 'double_dqn', 'd3qn']:
        for display in ['gray', 'color']:
            print(f"\n\nRunning ablation with {network!r}, for {display!r} environment.")

            color_ranges = color_ranges_gray if display == 'gray' else color_ranges_rgb
            kwargs = dict(kwargs_env=dict(display=display), color_ranges=color_ranges, network=network)
            run_dqn_experiment(**kwargs, mode='ablation')

    # Only allow combinations for limited gray region, since color values take a lot of time
    # (see `utils.calculate_colors` for estimation time).

    color_ranges = [
        (0, 255, 16), (0, 255, 16), (0, 255, 16)
    ]

    for network in ['dqn', 'dueling_dqn', 'double_dqn', 'd3qn']:
        for display in ['gray']:
            print(f"\n\nRunning combinations with {network!r}, for {display!r} environment.")

            kwargs = dict(kwargs_env=dict(display=display), color_ranges=color_ranges, network=network)
            run_dqn_experiment(**kwargs, mode='combined')
