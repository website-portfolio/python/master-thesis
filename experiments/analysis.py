from experiments.utils.storing import load_data_from_settings, load_from_folder
from experiments.utils.visualize import plot_data, plot_merge_data


def plot_separate(folders, merge=False, min_score=None):
    for folder in folders:
        results_recolor = load_from_folder(folder, results=True).results
        plot_data(results_recolor, merge=merge, min_score=min_score)


def plot_combined(folders, keys):
    results = {key: load_from_folder(folder, results=True).results for key, folder in zip(keys, folders)}
    plot_merge_data(results)


def plot_agents(mode='combined', min_score=None, display='gray'):
    folders = []
    for network in ['dqn', 'dueling_dqn', 'double_dqn', 'd3qn']:
        match = dict(
                mode=mode,
                env=dict(display=display, attention=None),
                dqn=dict(network=network)
        )
        folders.extend(load_data_from_settings(match, folder='baselines'))

    if display == 'color':
        return plot_separate(folders, min_score=min_score)
    if mode == 'ablation':
        return plot_combined(folders, keys=['dqn', 'dueling_dqn', 'double_dqn', 'd3qn'])
    return plot_separate(folders, merge=True, min_score=min_score)


def plot_attention(mode='ablation', min_score=None, display='gray', merge=True):
    folders = []

    for attention in [None, 'position', 'color', 'double']:
        match = dict(
                mode=mode,
                env=dict(display=display, attention=attention),
                dqn=dict(network=['dqn'])
        )
        folders.extend(load_data_from_settings(match, folder='attention' if attention else 'baselines'))

    if mode == 'ablation' and display == 'gray':
        return plot_combined(folders, keys=[None, 'position', 'color', 'double'])
    return plot_separate(folders, merge=merge, min_score=min_score)


def plot_augmentation(min_score=None):
    for order in [
        # ['crop'],
        # ['color_jitter'],
        ['cutout_color'],
        # ['crop', 'color_jitter'],
        # ['crop', 'cutout_color'],
        # ['cutout_color', 'color_jitter'],
        # ['crop', 'color_jitter', 'cutout_color'],
    ]:
        match = dict(
                mode='ablation',
                env=dict(display='color', attention=None, aug_kwargs=dict(order=tuple(order))),
                dqn=dict(network='dqn')
        )
        folders = load_data_from_settings(match, folder='Epsilon*')  # 'augmentation')
        print(f"Solving: {order}")
        plot_separate(folders, min_score=min_score)


def plot_inverse(mode, min_score=None, display='gray', keys=(None, 'position', 'color', 'double')):
    folders = []
    for key in keys:
        match = dict(
                mode=mode,
                env=dict(display=display, attention=key),
                dqn=dict(network=['dqn'])
        )
        folders.extend(load_data_from_settings(match, folder='inverse'))

    if display == 'color' or mode == 'combined':
        return plot_separate(folders, merge=bool(mode == 'combined'), min_score=min_score)
    return plot_combined(folders, keys=keys)


def plot_mask(mode, min_score=None, display='gray', keys=(None, 'position', 'color', 'double'), combine=False):
    folders = []
    for attention in keys:
        match = dict(
                mode=mode,
                env=dict(display=display, attention=attention, inverse=True),
                dqn=dict(network=['dqn'])
        )
        folders.extend(load_data_from_settings(match, folder='attention_mask' if attention else 'baselines'))

    if display == 'color' or mode == 'combined':
        return plot_separate(folders, merge=bool(mode == 'combined'), min_score=min_score)
    if combine:
        return plot_combined(folders, keys)
    return plot_separate(folders, min_score=min_score)


if __name__ == '__main__':
    # plot_agents(mode='ablation', min_score=0.4, display='gray')
    # plot_attention(mode='ablation', min_score=None, display='gray', merge=False)
    plot_augmentation(min_score=.4)
    # plot_inverse(mode='ablation', display='color')
    # plot_mask(mode='ablation', display='gray', combine=True)
    pass
