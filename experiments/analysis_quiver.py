from core.environment import create_env
from core.vis.recoloring import RecoloringQuiverPlot
from core.vis.video import VideoReader
from experiments.utils.storing import load_data_from_settings, load_from_folder


def get_env_model(network, folder='baselines', display='gray'):
    match = dict(
            mode='ablation',
            env=dict(display=display, attention=None),
            dqn=dict(network=network)
    )
    for folder in load_data_from_settings(match, folder=folder):
        data = load_from_folder(folder, settings=True, agent=True, results=False)
        env, _ = create_env(**data.settings['env'])
        yield env, data.agent


# Color ranges, start, end, number of points
COLOR_RANGES_GRAY = [
    (0, 255, 4),  # Background
    (0, 255, 4),  # Ball (goal)
    (0, 255, 4),  # Paddle (player)
]

COLOR_RANGES_RGB = [
    ((0, 0, 0), (255, 255, 255), (2, 2, 2)),  # Background
    ((0, 0, 0), (255, 255, 255), (2, 2, 2)),  # Ball (goal)
    ((0, 0, 0), (255, 255, 255), (2, 2, 2)),  # Paddle (player)
]

if __name__ == '__main__':
    for env, agent in get_env_model(network='dqn', display='gray'):
        color_ranges = COLOR_RANGES_GRAY if env.display_mode == 'gray' else COLOR_RANGES_RGB
        sprites = dict(background=env.background, ball=env.goal, paddle=env.player)

        plotter = RecoloringQuiverPlot(env, agent, sprites, color_ranges)
        # path = plotter.record()  # Create changing colors
        path = plotter.play()  # Play a single game (default colors)

        # read the recorded video, and display it slowly
        video = VideoReader(path=path)
        video.show(fps=1)
