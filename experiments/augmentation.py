from experiments.utils.running import run_dqn_experiment

if __name__ == '__main__':
    color_ranges = [
        ((0, 0, 0), (255, 255, 255), (8, 8, 8)),  # Background
        ((0, 0, 0), (255, 255, 255), (8, 8, 8)),  # Ball (goal)
        ((0, 0, 0), (255, 255, 255), (8, 8, 8)),  # Paddle (player)
    ]

    for display in ['color']:
        for network in ['dqn']:
            for order in [
                ['crop'],
                ['color_jitter'],
                ['cutout_color'],
                ['crop', 'color_jitter'],
                ['crop', 'cutout_color'],
                ['cutout_color', 'color_jitter'],
                ['crop', 'color_jitter', 'cutout_color'],
            ]:
                aug_kwargs = dict(
                        crop=18,
                        gray_p=.3,
                        flip_p=.2,
                        rotation_p=.3,
                        cutout=(2, 5),
                        cutout_value=0,
                        cutout_color=(2, 5),
                        color_jitter=dict(p=.3, hue=(0, 255), saturation=(0, 255), value=(0, 255)),
                        translate=dict(p=.3, dx=(-5, 5), dy=(-5, 5)),
                        order=order
                )

                print(f"\n\nRunning ablation with {network!r}, for {display!r} environment.")

                kwargs = dict(kwargs_env=dict(display=display, aug_kwargs=aug_kwargs),
                              color_ranges=color_ranges, network=network)
                run_dqn_experiment(**kwargs, mode='ablation', runs=100_000)
